package view.container;

import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Bounds;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.CacheHint;
import javafx.scene.Cursor;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.SnapshotResult;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.Effect;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.InputMethodRequests;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.input.ZoomEvent;
import javafx.scene.transform.Transform;
import javafx.util.Callback;

public class MyGroup{

    protected Group group;

    public MyGroup() {
        group = new Group();

    }

    public MyGroup(Node[] nodes) {
        group = new Group(nodes);
    }

    protected void setAutoSizeChildren(boolean bln) {
        group.setAutoSizeChildren(bln);
    }

    protected boolean isAutoSizeChildren() {
        return group.isAutoSizeChildren();
    }

    protected BooleanProperty autoSizeChildrenProperty() {
        return group.autoSizeChildrenProperty();
    }

    protected ObservableList<Node> getChildren() {
        return group.getChildren();
    }

    public double prefWidth(double d) {
        return group.prefWidth(d);
    }

    public double prefHeight(double d) {
        return group.prefHeight(d);
    }

    protected ObservableList<Node> getChildrenUnmodifiable() {
        return group.getChildrenUnmodifiable();
    }

    public Node lookup(String string) {
        return group.lookup(string);
    }

    public boolean isNeedsLayout() {
        return group.isNeedsLayout();
    }

    public ReadOnlyBooleanProperty needsLayoutProperty() {
        return group.needsLayoutProperty();
    }

    public void requestLayout() {
        group.requestLayout();
    }

    public double minWidth(double d) {
        return group.minWidth(d);
    }

    public double minHeight(double d) {
        return group.minHeight(d);
    }

    public double getBaselineOffset() {
        return group.getBaselineOffset();
    }

    public void layout() {
        group.layout();
    }

    public ObservableList<String> getStylesheets() {
        return group.getStylesheets();
    }

    public Group getPNode() {
        return group;
    }

    public ObservableMap<Object, Object> getProperties() {
        return group.getProperties();
    }

    public boolean hasProperties() {
        return group.hasProperties();
    }

    public void setUserData(Object o) {

    }

    public Object getUserData() {
        return group.getUserData();
    }

    void setParent(Parent parent) {

    }

    public Parent getParent() {
        return group.getParent();
    }

    public ReadOnlyObjectProperty<Parent> parentProperty() {
        return group.parentProperty();
    }

    public Scene getScene() {
        return group.getScene();
    }

    public ReadOnlyObjectProperty<Scene> sceneProperty() {
        return group.sceneProperty();
    }

    public void setId(String string) {
        group.setId(string);
    }

    public String getId() {
        return group.getId();
    }

    public StringProperty idProperty() {
        return group.idProperty();
    }

    public ObservableList<String> getStyleClass() {
        return group.getStyleClass();
    }

    public void setStyle(String string) {
        group.setStyle(string);
    }

    public String getStyle() {
        return group.getStyle();
    }

    public StringProperty styleProperty() {
        return group.styleProperty();
    }

    public void setVisible(boolean bln) {
        group.setVisible(bln);
    }

    public boolean isVisible() {
        return group.isVisible();
    }

    public BooleanProperty visibleProperty() {
        return group.visibleProperty();
    }

    public void setCursor(Cursor cursor) {
        group.setCursor(cursor);
    }

    public Cursor getCursor() {
        return group.getCursor();
    }

    public ObjectProperty<Cursor> cursorProperty() {
        return group.cursorProperty();
    }

    public void setOpacity(double d) {
        group.setOpacity(d);
    }

    public double getOpacity() {
        return group.getOpacity();
    }

    public DoubleProperty opacityProperty() {
        return group.opacityProperty();
    }

    public void setBlendMode(BlendMode bm) {
        group.setBlendMode(bm);
    }

    public BlendMode getBlendMode() {
        return group.getBlendMode();

    }

    public ObjectProperty<BlendMode> blendModeProperty() {
        return group.blendModeProperty();
    }

    public void setClip(Node node) {
        group.setClip(node);
    }

    public Node getClip() {
        return group.getClip();
    }

    public ObjectProperty<Node> clipProperty() {
        return group.clipProperty();
    }

    public void setCache(boolean bln) {
        group.setCache(bln);
    }

    public boolean isCache() {
        return group.isCache();
    }

    public BooleanProperty cacheProperty() {
        return group.cacheProperty();
    }

    public void setCacheHint(CacheHint ch) {
        group.setCacheHint(ch);
    }

    public CacheHint getCacheHint() {
        return group.getCacheHint();
    }

    public ObjectProperty<CacheHint> cacheHintProperty() {
        return group.cacheHintProperty();
    }

    public void setEffect(Effect effect) {
        group.setEffect(effect);
    }

    public Effect getEffect() {
        return group.getEffect();

    }

    public ObjectProperty<Effect> effectProperty() {
        return group.effectProperty();
    }

    public void setDepthTest(DepthTest dt) {
        group.setDepthTest(dt);
    }

    public DepthTest getDepthTest() {
        return group.getDepthTest();
    }

    public ObjectProperty<DepthTest> depthTestProperty() {
        return group.depthTestProperty();
    }

    public void setDisable(boolean bln) {
        group.setDisable(bln);
    }

    public boolean isDisable() {
        return group.isDisable();

    }

    public BooleanProperty disableProperty() {
        return group.disableProperty();
    }

    public void setPickOnBounds(boolean bln) {
        group.setPickOnBounds(bln);
    }

    public boolean isPickOnBounds() {
        return group.isPickOnBounds();
    }

    public BooleanProperty pickOnBoundsProperty() {
        return group.pickOnBoundsProperty();
    }

    public boolean isDisabled() {
        return group.isDisabled();
    }

    public ReadOnlyBooleanProperty disabledProperty() {
        return group.disabledProperty();
    }

    public Set<Node> lookupAll(String string) {
        return group.lookupAll(string);
    }

    public WritableImage snapshot(SnapshotParameters sp, WritableImage wi) {
        return group.snapshot(sp, wi);
    }

    public void snapshot(Callback<SnapshotResult, Void> clbck, SnapshotParameters sp, WritableImage wi) {
        group.snapshot(clbck, sp, wi);
    }

    public void setOnDragEntered(EventHandler<? super DragEvent> eh) {
        group.setOnDragEntered(eh);
    }

    public EventHandler<? super DragEvent> getOnDragEntered() {
        return group.getOnDragEntered();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragEnteredProperty() {
        return group.onDragEnteredProperty();
    }

    public void setOnDragExited(EventHandler<? super DragEvent> eh) {
        group.setOnDragExited(eh);
    }

    public EventHandler<? super DragEvent> getOnDragExited() {
        return group.getOnDragExited();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragExitedProperty() {
        return group.onDragExitedProperty();
    }

    public void setOnDragOver(EventHandler<? super DragEvent> eh) {
        group.setOnDragOver(eh);
    }

    public EventHandler<? super DragEvent> getOnDragOver() {
        return group.getOnDragOver();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragOverProperty() {
        return group.onDragOverProperty();
    }

    public void setOnDragDropped(EventHandler<? super DragEvent> eh) {
        group.setOnDragDropped(eh);
    }

    public EventHandler<? super DragEvent> getOnDragDropped() {
        return group.getOnDragDropped();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragDroppedProperty() {
        return group.onDragDroppedProperty();
    }

    public void setOnDragDone(EventHandler<? super DragEvent> eh) {
        group.setOnDragDone(eh);
    }

    public EventHandler<? super DragEvent> getOnDragDone() {
        return group.getOnDragDone();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragDoneProperty() {
        return group.onDragDoneProperty();
    }

    public Dragboard startDragAndDrop(TransferMode[] tms) {
        return group.startDragAndDrop(tms);
    }

    public void startFullDrag() {
        group.startFullDrag();
    }

    public void setManaged(boolean bln) {
        group.setManaged(bln);
    }

    public boolean isManaged() {
        return group.isManaged();
    }

    public BooleanProperty managedProperty() {
        return group.managedProperty();
    }

    public void setLayoutX(double d) {
        group.setLayoutX(d);
    }

    public double getLayoutX() {
        return group.getLayoutX();
    }

    public DoubleProperty layoutXProperty() {
        return group.layoutXProperty();
    }

    public void setLayoutY(double d) {
        group.setLayoutY(d);
    }

    public double getLayoutY() {
        return group.getLayoutY();
    }

    public DoubleProperty layoutYProperty() {
        return group.layoutYProperty();
    }

    public void relocate(double d, double d1) {
        group.relocate(d, d1);
    }

    public boolean isResizable() {
        return group.isResizable();
    }

    public Orientation getContentBias() {
        return group.getContentBias();
    }

    public double maxWidth(double d) {
        return group.maxWidth(d);
    }

    public double maxHeight(double d) {
        return group.maxHeight(d);
    }

    public void resize(double d, double d1) {
        group.resize(d, d1);
    }

    public void autosize() {
        group.autosize();
    }

    public void resizeRelocate(double x, double y, double w, double h) {
        group.resizeRelocate(x, y, w, h);
    }

    public Bounds getBoundsInParent() {
        return group.getBoundsInParent();
    }

    public ReadOnlyObjectProperty<Bounds> boundsInParentProperty() {
        return group.boundsInParentProperty();
    }

    public Bounds getBoundsInLocal() {
        return group.getBoundsInLocal();
    }

    public ReadOnlyObjectProperty<Bounds> boundsInLocalProperty() {
        return group.boundsInLocalProperty();
    }

    public Bounds getLayoutBounds() {
        return group.getLayoutBounds();
    }

    public ReadOnlyObjectProperty<Bounds> layoutBoundsProperty() {
        return group.layoutBoundsProperty();
    }

    public boolean contains(double x, double y) {
        return group.contains(x, y);
    }

    public boolean contains(Point2D xy) {
        return group.contains(xy);
    }

    public boolean intersects(double x, double y, double w, double h) {
        return group.intersects(x, y, w, h);
    }

    public boolean intersects(Bounds bounds) {
        return group.intersects(bounds);
    }

    public Point2D sceneToLocal(double d, double d1) {
        return group.sceneToLocal(d, d1);
    }

    public Point2D sceneToLocal(Point2D pd) {
        return group.sceneToLocal(pd);
    }

    public Bounds sceneToLocal(Bounds bounds) {
        return group.sceneToLocal(bounds);
    }

    public Point2D localToScene(double d, double d1) {
        return group.localToParent(d, d1);
    }

    public Point2D localToScene(Point2D pd) {
        return group.localToParent(pd);
    }

    public Bounds localToScene(Bounds bounds) {
        return group.localToParent(bounds);
    }

    public Point2D parentToLocal(double d, double d1) {
        return group.parentToLocal(d, d1);
    }

    public Point2D parentToLocal(Point2D pd) {
        return group.parentToLocal(pd);
    }

    public Bounds parentToLocal(Bounds bounds) {
        return group.parentToLocal(bounds);
    }

    public Point2D localToParent(double d, double d1) {
        return group.localToParent(d, d1);
    }

    public Point2D localToParent(Point2D pd) {
        return group.localToParent(pd);
    }

    public Bounds localToParent(Bounds bounds) {
        return group.localToParent(bounds);
    }

    public ObservableList<Transform> getTransforms() {
        return group.getTransforms();
    }

    public void setTranslateX(double d) {
        group.setTranslateX(d);
    }

    public double getTranslateX() {
        return group.getTranslateX();
    }

    public DoubleProperty translateXProperty() {
        return group.translateXProperty();
    }

    public void setTranslateY(double d) {
        group.setTranslateY(d);
    }

    public double getTranslateY() {
        return group.getTranslateY();
    }

    public DoubleProperty translateYProperty() {
        return group.translateYProperty();
    }

    public void setTranslateZ(double d) {
        group.setTranslateZ(d);
    }

    public double getTranslateZ() {
        return group.getTranslateZ();
    }

    public DoubleProperty translateZProperty() {
        return group.translateZProperty();
    }

    public void setScaleX(double d) {
        group.setScaleX(d);
    }

    public double getScaleX() {
        return group.getScaleX();
    }

    public DoubleProperty scaleXProperty() {
        return group.scaleXProperty();
    }

    public void setScaleY(double d) {
        group.setScaleY(d);

    }

    public double getScaleY() {
        return group.getScaleY();
    }

    public DoubleProperty scaleYProperty() {
        return group.scaleYProperty();
    }

    public void setScaleZ(double d) {
        group.setScaleZ(d);
    }

    public double getScaleZ() {
        return group.getScaleZ();
    }

    public DoubleProperty scaleZProperty() {
        return scaleZProperty();
    }

    public void setRotate(double d) {
        group.setRotate(d);
    }

    public double getRotate() {
        return group.getRotate();
    }

    public DoubleProperty rotateProperty() {
        return group.rotateProperty();

    }

    public void setRotationAxis(Point3D pd) {
        group.setRotationAxis(pd);
    }

    public Point3D getRotationAxis() {
        return group.getRotationAxis();
    }

    public ObjectProperty<Point3D> rotationAxisProperty() {
        return group.rotationAxisProperty();
    }

    public ReadOnlyObjectProperty<Transform> localToParentTransformProperty() {
        return group.localToParentTransformProperty();
    }

    public Transform getLocalToParentTransform() {
        return group.getLocalToParentTransform();
    }

    public ReadOnlyObjectProperty<Transform> localToSceneTransformProperty() {
        return group.localToSceneTransformProperty();
    }

    public Transform getLocalToSceneTransform() {
        return group.getLocalToSceneTransform();
    }

    public void setMouseTransparent(boolean bln) {
        group.setMouseTransparent(bln);
    }

    public boolean isMouseTransparent() {
        return group.isMouseTransparent();
    }

    public BooleanProperty mouseTransparentProperty() {
        return group.mouseTransparentProperty();
    }

    public boolean isHover() {
        return group.isHover();
    }

    public ReadOnlyBooleanProperty hoverProperty() {
        return group.hoverProperty();
    }

    public boolean isPressed() {
        return group.isPressed();
    }

    public ReadOnlyBooleanProperty pressedProperty() {
        return group.pressedProperty();
    }

    public void setOnContextMenuRequested(EventHandler<? super ContextMenuEvent> eh) {
        group.setOnContextMenuRequested(eh);
    }

    public EventHandler<? super ContextMenuEvent> getOnContextMenuRequested() {
        return group.getOnContextMenuRequested();
    }

    public ObjectProperty<EventHandler<? super ContextMenuEvent>> onContextMenuRequestedProperty() {
        return group.onContextMenuRequestedProperty();
    }

    public void setOnMouseClicked(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseClicked(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseClicked() {
        return group.getOnMouseClicked();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseClickedProperty() {
        return group.onMouseClickedProperty();
    }

    public void setOnMouseDragged(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseDragged(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseDragged() {
        return group.getOnMouseDragged();

    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseDraggedProperty() {
        return group.onMouseDraggedProperty();
    }

    public void setOnMouseEntered(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseEntered(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseEntered() {
        return group.getOnMouseEntered();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseEnteredProperty() {
        return group.onMouseEnteredProperty();
    }

    public void setOnMouseExited(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseExited(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseExited() {
        return group.getOnMouseExited();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseExitedProperty() {
        return group.onMouseExitedProperty();
    }

    public void setOnMouseMoved(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseMoved(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseMoved() {
        return group.getOnMouseMoved();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseMovedProperty() {
        return group.onMouseMovedProperty();
    }

    public void setOnMousePressed(EventHandler<? super MouseEvent> eh) {
        group.setOnMousePressed(eh);
    }

    public EventHandler<? super MouseEvent> getOnMousePressed() {
        return group.getOnMousePressed();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMousePressedProperty() {
        return group.onMousePressedProperty();
    }

    public void setOnMouseReleased(EventHandler<? super MouseEvent> eh) {
        group.setOnMouseReleased(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseReleased() {
        return group.getOnMouseReleased();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseReleasedProperty() {
        return group.onMouseReleasedProperty();
    }

    public void setOnDragDetected(EventHandler<? super MouseEvent> eh) {
        group.setOnDragDetected(eh);
    }

    public EventHandler<? super MouseEvent> getOnDragDetected() {
        return group.getOnDragDetected();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onDragDetectedProperty() {
        return group.onDragDetectedProperty();
    }

    public void setOnMouseDragOver(EventHandler<? super MouseDragEvent> eh) {
        group.setOnMouseDragOver(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragOver() {
        return group.getOnMouseDragOver();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragOverProperty() {
        return group.onMouseDragOverProperty();
    }

    public void setOnMouseDragReleased(EventHandler<? super MouseDragEvent> eh) {
        group.setOnMouseDragReleased(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragReleased() {
        return group.getOnMouseDragReleased();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragReleasedProperty() {
        return group.onMouseDragReleasedProperty();
    }

    public void setOnMouseDragEntered(EventHandler<? super MouseDragEvent> eh) {
        group.setOnMouseDragEntered(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragEntered() {
        return group.getOnMouseDragEntered();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragEnteredProperty() {
        return group.onMouseDragEnteredProperty();
    }

    public void setOnMouseDragExited(EventHandler<? super MouseDragEvent> eh) {
        group.setOnMouseDragExited(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragExited() {
        return group.getOnMouseDragExited();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragExitedProperty() {
        return group.onMouseDragExitedProperty();
    }

    public void setOnScrollStarted(EventHandler<? super ScrollEvent> eh) {
        group.setOnScroll(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScrollStarted() {
        return group.getOnScrollStarted();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollStartedProperty() {
        return group.onScrollStartedProperty();
    }

    public void setOnScroll(EventHandler<? super ScrollEvent> eh) {
        group.setOnScroll(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScroll() {
        return group.getOnScroll();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollProperty() {
        return group.onScrollProperty();
    }

    public void setOnScrollFinished(EventHandler<? super ScrollEvent> eh) {
        group.setOnScrollFinished(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScrollFinished() {
        return group.getOnScrollFinished();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollFinishedProperty() {
        return group.onScrollFinishedProperty();
    }

    public void setOnRotationStarted(EventHandler<? super RotateEvent> eh) {
        group.setOnRotationStarted(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotationStarted() {
        return group.getOnRotationStarted();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotationStartedProperty() {
        return group.onRotationStartedProperty();
    }

    public void setOnRotate(EventHandler<? super RotateEvent> eh) {
        group.setOnRotate(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotate() {
        return group.getOnRotate();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotateProperty() {
        return group.onRotateProperty();
    }

    public void setOnRotationFinished(EventHandler<? super RotateEvent> eh) {
        group.setOnRotationFinished(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotationFinished() {
        return group.getOnRotationFinished();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotationFinishedProperty() {
        return group.onRotationFinishedProperty();
    }

    public void setOnZoomStarted(EventHandler<? super ZoomEvent> eh) {
        group.setOnZoomStarted(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoomStarted() {
        return group.getOnZoomStarted();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomStartedProperty() {
        return group.onZoomStartedProperty();
    }

    public void setOnZoom(EventHandler<? super ZoomEvent> eh) {
        group.setOnZoom(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoom() {
        return group.getOnZoom();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomProperty() {
        return group.onZoomProperty();
    }

    public void setOnZoomFinished(EventHandler<? super ZoomEvent> eh) {
        group.setOnZoomFinished(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoomFinished() {
        return group.getOnZoomFinished();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomFinishedProperty() {
        return group.onZoomFinishedProperty();
    }

    public void setOnSwipeUp(EventHandler<? super SwipeEvent> eh) {
        group.setOnSwipeUp(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeUp() {
        return group.getOnSwipeUp();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeUpProperty() {
        return group.onSwipeUpProperty();
    }

    public void setOnSwipeDown(EventHandler<? super SwipeEvent> eh) {
        group.setOnSwipeDown(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeDown() {
        return group.getOnSwipeDown();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeDownProperty() {
        return group.onSwipeDownProperty();
    }

    public void setOnSwipeLeft(EventHandler<? super SwipeEvent> eh) {
        group.setOnSwipeLeft(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeLeft() {
        return group.getOnSwipeLeft();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeLeftProperty() {
        return group.onSwipeLeftProperty();
    }

    public void setOnSwipeRight(EventHandler<? super SwipeEvent> eh) {
        group.setOnSwipeRight(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeRight() {
        return group.getOnSwipeRight();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeRightProperty() {
        return group.onSwipeRightProperty();
    }

    public void setOnTouchPressed(EventHandler<? super TouchEvent> eh) {
        group.setOnTouchPressed(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchPressed() {
        return group.getOnTouchPressed();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchPressedProperty() {
        return group.onTouchPressedProperty();
    }

    public void setOnTouchMoved(EventHandler<? super TouchEvent> eh) {
        group.setOnTouchMoved(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchMoved() {
        return group.getOnTouchMoved();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchMovedProperty() {
        return group.onTouchMovedProperty();
    }

    public void setOnTouchReleased(EventHandler<? super TouchEvent> eh) {
        group.setOnTouchReleased(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchReleased() {
        return group.getOnTouchReleased();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchReleasedProperty() {
        return group.onTouchReleasedProperty();
    }

    public void setOnTouchStationary(EventHandler<? super TouchEvent> eh) {
        group.setOnTouchStationary(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchStationary() {
        return group.getOnTouchStationary();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchStationaryProperty() {
        return group.onTouchStationaryProperty();
    }

    public void setOnKeyPressed(EventHandler<? super KeyEvent> eh) {
        group.setOnKeyPressed(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyPressed() {
        return group.getOnKeyPressed();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyPressedProperty() {
        return group.onKeyPressedProperty();
    }

    public void setOnKeyReleased(EventHandler<? super KeyEvent> eh) {
        group.setOnKeyReleased(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyReleased() {
        return group.getOnKeyReleased();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyReleasedProperty() {
        return group.onKeyReleasedProperty();
    }

    public void setOnKeyTyped(EventHandler<? super KeyEvent> eh) {
        group.setOnKeyTyped(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyTyped() {
        return group.getOnKeyTyped();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyTypedProperty() {
        return group.onKeyTypedProperty();
    }

    public void setOnInputMethodTextChanged(EventHandler<? super InputMethodEvent> eh) {
        group.setOnInputMethodTextChanged(eh);
    }

    public EventHandler<? super InputMethodEvent> getOnInputMethodTextChanged() {
        return group.getOnInputMethodTextChanged();
    }

    public ObjectProperty<EventHandler<? super InputMethodEvent>> onInputMethodTextChangedProperty() {
        return group.onInputMethodTextChangedProperty();
    }

    public void setInputMethodRequests(InputMethodRequests imr) {
        group.setInputMethodRequests(imr);
    }

    public InputMethodRequests getInputMethodRequests() {
        return group.getInputMethodRequests();
    }

    public ObjectProperty<InputMethodRequests> inputMethodRequestsProperty() {
        return group.inputMethodRequestsProperty();
    }

    public boolean isFocused() {
        return group.isFocused();
    }

    public ReadOnlyBooleanProperty focusedProperty() {
        return group.focusedProperty();
    }

    public void setFocusTraversable(boolean bln) {
        group.setFocusTraversable(bln);
    }

    public boolean isFocusTraversable() {
        return group.isFocusTraversable();
    }

    public BooleanProperty focusTraversableProperty() {
        return group.focusTraversableProperty();
    }

    public void requestFocus() {
        group.requestFocus();
    }

    public String toString() {
        return group.toString();
    }

    public void setEventDispatcher(EventDispatcher ed) {
        group.setEventDispatcher(ed);
    }

    public EventDispatcher getEventDispatcher() {
        return group.getEventDispatcher();
    }

    public ObjectProperty<EventDispatcher> eventDispatcherProperty() {
        return group.eventDispatcherProperty();
    }

    public <T extends Event> void addEventHandler(EventType<T> et, EventHandler<? super T> eh) {
        group.addEventHandler(et, eh);
    }

    public <T extends Event> void removeEventHandler(EventType<T> et, EventHandler<? super T> eh) {
        group.removeEventHandler(et, eh);
    }

    public <T extends Event> void addEventFilter(EventType<T> et, EventHandler<? super T> eh) {
        group.addEventFilter(et, eh);
    }

    public <T extends Event> void removeEventFilter(EventType<T> et, EventHandler<? super T> eh) {
        group.removeEventFilter(et, eh);
    }

    public EventDispatchChain buildEventDispatchChain(EventDispatchChain edc) {
        return group.buildEventDispatchChain(edc);
    }

    public void fireEvent(Event event) {
        group.fireEvent(event);
    }
}


 