/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.container;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.ZoomEvent;

public class MyGroupCover extends MyGroup {

    private EventHandler<? super KeyEvent> onKeyTyped;
    private EventHandler<? super KeyEvent> onKeyReleased;
    private EventHandler<? super KeyEvent> onKeyPressed;
    private EventHandler<? super TouchEvent> onTouchStationary;
    private EventHandler<? super TouchEvent> onTouchReleased;
    private EventHandler<? super TouchEvent> onTouchMoved;
    private EventHandler<? super TouchEvent> onTouchPressed;
    private EventHandler<? super SwipeEvent> onSwipeRight;
    private EventHandler<? super SwipeEvent> onSwipeLeft;
    private EventHandler<? super SwipeEvent> onSwipeDown;
    private EventHandler<? super SwipeEvent> onSwipeUp;
    private EventHandler<? super ZoomEvent> onZoomFinished;
    private EventHandler<? super ZoomEvent> onZoom;
    private EventHandler<? super ZoomEvent> onZoomStarted;
    private EventHandler<? super RotateEvent> onRotationFinished;
    private EventHandler<? super RotateEvent> onRotate;
    private EventHandler<? super RotateEvent> onRotationStarted;
    private EventHandler<? super ScrollEvent> onScrollFinished;
    private EventHandler<? super ScrollEvent> onScroll;
    private EventHandler<? super ScrollEvent> onScrollStarted;
    private EventHandler<? super MouseDragEvent> onMouseDragExited;
    private EventHandler<? super MouseDragEvent> onMouseDragEntered;
    private EventHandler<? super MouseDragEvent> onMouseDragReleased;
    private EventHandler<? super MouseDragEvent> onMouseDragOver;
    private EventHandler<? super MouseEvent> onDragDetected;
    private EventHandler<? super MouseEvent> onMouseReleased;
    private EventHandler<? super MouseEvent> onMousePressed;
    private EventHandler<? super MouseEvent> onMouseMoved;
    private EventHandler<? super MouseEvent> onMouseExited;
    private EventHandler<? super MouseEvent> onMouseEntered;
    private EventHandler<? super MouseEvent> onMouseDragged;
    private EventHandler<? super MouseEvent> onMouseClicked;
    private EventHandler<? super DragEvent> onDragDone;
    private EventHandler<? super DragEvent> onDragDropped;
    private EventHandler<? super DragEvent> onDragOver;
    private EventHandler<? super DragEvent> onDragExited;
    private EventHandler<? super DragEvent> onDragEntered;

    private boolean onKeyTypedEnable = true;
    private boolean onKeyReleasedEnable = true;
    private boolean onKeyPressedEnable = true;
    private boolean onTouchStationaryEnable = true;
    private boolean onTouchReleasedEnable = true;
    private boolean onTouchMovedEnable = true;
    private boolean onTouchPressedEnable = true;
    private boolean onSwipeRightEnable = true;
    private boolean onSwipeLeftEnable = true;
    private boolean onSwipeDownEnable = true;
    private boolean onSwipeUpEnable = true;
    private boolean onZoomFinishedEnable = true;
    private boolean onZoomEnable = true;
    private boolean onZoomStartedEnable = true;
    private boolean onRotationFinishedEnable = true;
    private boolean onRotateEnable = true;
    private boolean onRotationStartedEnable = true;
    private boolean onScrollFinishedEnable = true;
    private boolean onScrollEnable = true;
    private boolean onScrollStartedEnable = true;
    private boolean onMouseDragExitedEnable = true;
    private boolean onMouseDragEnteredEnable = true;
    private boolean onMouseDragReleasedEnable = true;
    private boolean onMouseDragOverEnable = true;
    private boolean onDragDetectedEnable = true;
    private boolean onMouseReleasedEnable = true;
    private boolean onMousePressedEnable = true;
    private boolean onMouseMovedEnable = true;
    private boolean onMouseExitedEnable = true;
    private boolean onMouseEnteredEnable = true;
    private boolean onMouseDraggedEnable = true;
    private boolean onMouseClickedEnable = true;
    private boolean onDragDoneEnable = true;
    private boolean onDragDroppedEnable = true;
    private boolean onDragOverEnable = true;
    private boolean onDragExitedEnable = true;
    private boolean onDragEnteredEnable = true;

    public void setOnKeyTypedEnable(boolean en) {
        onKeyTypedEnable = en;
    }

    public void setOnKeyReleasedEnable(boolean en) {
        onKeyReleasedEnable = en;
    }

    public void setOnKeyPressedEnable(boolean en) {
        onKeyPressedEnable = en;
    }

    public void setOnTouchStationaryEnable(boolean en) {
        onTouchStationaryEnable = en;
    }

    public void setOnTouchReleasedEnable(boolean en) {
        onTouchReleasedEnable = en;
    }

    public void setOnTouchMovedEnable(boolean en) {
        onTouchMovedEnable = en;
    }

    public void setOnTouchPressedEnable(boolean en) {
        onTouchPressedEnable = en;
    }

    public void setOnSwipeRightEnable(boolean en) {
        onSwipeRightEnable = en;
    }

    public void setOnSwipeLeftEnable(boolean en) {
        onSwipeLeftEnable = en;
    }

    public void setOnSwipeDownEnable(boolean en) {
        onSwipeDownEnable = en;
    }

    public void setOnSwipeUpEnable(boolean en) {
        onSwipeUpEnable = en;
    }

    public void setOnZoomFinishedEnable(boolean en) {
        onZoomFinishedEnable = en;
    }

    public void setOnZoomEnable(boolean en) {
        onZoomEnable = en;
    }

    public void setOnZoomStartedEnable(boolean en) {
        onZoomStartedEnable = en;
    }

    public void setOnRotationFinishedEnable(boolean en) {
        onRotationFinishedEnable = en;
    }

    public void setOnRotateEnable(boolean en) {
        onRotateEnable = en;
    }

    public void setOnRotationStartedEnable(boolean en) {
        onRotationStartedEnable = en;
    }

    public void setOnScrollFinishedEnable(boolean en) {
        onScrollFinishedEnable = en;
    }

    public void setOnScrollEnable(boolean en) {
        onScrollEnable = en;
    }

    public void setOnScrollStartedEnable(boolean en) {
        onScrollStartedEnable = en;
    }

    public void setOnMouseDragExitedEnable(boolean en) {
        onMouseDragExitedEnable = en;
    }

    public void setOnMouseDragEnteredEnable(boolean en) {
        onMouseDragEnteredEnable = en;
    }

    public void setOnMouseDragReleasedEnable(boolean en) {
        onMouseDragReleasedEnable = en;
    }

    public void setOnMouseDragOverEnable(boolean en) {
        onMouseDragOverEnable = en;
    }

    public void setOnDragDetectedEnable(boolean en) {
        onDragDetectedEnable = en;
    }

    public void setOnMouseReleasedEnable(boolean en) {
        onMouseReleasedEnable = en;
    }

    public void setOnMousePressedEnable(boolean en) {
        onMousePressedEnable = en;
    }

    public void setOnMouseMovedEnable(boolean en) {
        onMouseMovedEnable = en;
    }

    public void setOnMouseExitedEnable(boolean en) {
        onMouseExitedEnable = en;
    }

    public void setOnMouseEnteredEnable(boolean en) {
        onMouseEnteredEnable = en;
    }

    public void setOnMouseDraggedEnable(boolean en) {
        onMouseDraggedEnable = en;
    }

    public void setOnMouseClickedEnable(boolean en) {
        onMouseClickedEnable = en;
    }

    public void setOnDragDoneEnable(boolean en) {
        onDragDoneEnable = en;
    }

    public void setOnDragDroppedEnable(boolean en) {
        onDragDroppedEnable = en;
    }

    public void setOnDragOverEnable(boolean en) {
        onDragOverEnable = en;
    }

    public void setOnDragExitedEnable(boolean en) {
        onDragExitedEnable = en;
    }

    public boolean isOnKeyTypedEnable() {
        return onKeyTypedEnable;
    }

    public boolean isOnKeyReleasedEnable() {
        return onKeyReleasedEnable;
    }

    public boolean isOnKeyPressedEnable() {
        return onKeyPressedEnable;
    }

    public boolean isOnTouchStationaryEnable() {
        return onTouchStationaryEnable;
    }

    public boolean isOnTouchReleasedEnable() {
        return onTouchReleasedEnable;
    }

    public boolean isOnTouchMovedEnable() {
        return onTouchMovedEnable;
    }

    public boolean isOnTouchPressedEnable() {
        return onTouchPressedEnable;
    }

    public boolean isOnSwipeRightEnable() {
        return onSwipeRightEnable;
    }

    public boolean isOnSwipeLeftEnable() {
        return onSwipeLeftEnable;
    }

    public boolean isOnSwipeDownEnable() {
        return onSwipeDownEnable;
    }

    public boolean isOnSwipeUpEnable() {
        return onSwipeUpEnable;
    }

    public boolean isOnZoomFinishedEnable() {
        return onZoomFinishedEnable;
    }

    public boolean isOnZoomEnable() {
        return onZoomEnable;
    }

    public boolean isOnZoomStartedEnable() {
        return onZoomStartedEnable;
    }

    public boolean isOnRotationFinishedEnable() {
        return onRotationFinishedEnable;
    }

    public boolean isOnRotateEnable() {
        return onRotateEnable;
    }

    public boolean isOnRotationStartedEnable() {
        return onRotationStartedEnable;
    }

    public boolean isOnScrollFinishedEnable() {
        return onScrollFinishedEnable;
    }

    public boolean isOnScrollEnable() {
        return onScrollEnable;
    }

    public boolean isOnScrollStartedEnable() {
        return onScrollStartedEnable;
    }

    public boolean isOnMouseDragExitedEnable() {
        return onMouseDragExitedEnable;
    }

    public boolean isOnMouseDragEnteredEnable() {
        return onMouseDragEnteredEnable;
    }

    public boolean isOnMouseDragReleasedEnable() {
        return onMouseDragReleasedEnable;
    }

    public boolean isOnMouseDragOverEnable() {
        return onMouseDragOverEnable;
    }

    public boolean isOnDragDetectedEnable() {
        return onDragDetectedEnable;
    }

    public boolean isOnMouseReleasedEnable() {
        return onMouseReleasedEnable;
    }

    public boolean isOnMousePressedEnable() {
        return onMousePressedEnable;
    }

    public boolean isOnMouseMovedEnable() {
        return onMouseMovedEnable;
    }

    public boolean isOnMouseExitedEnable() {
        return onMouseExitedEnable;
    }

    public boolean isOnMouseEnteredEnable() {
        return onMouseEnteredEnable;
    }

    public boolean isOnMouseDraggedEnable() {
        return onMouseDraggedEnable;
    }

    public boolean isOnMouseClickedEnable() {
        return onMouseClickedEnable;
    }

    public boolean isOnDragDoneEnable() {
        return onDragDoneEnable;
    }

    public boolean isOnDragDroppedEnable() {
        return onDragDroppedEnable;
    }

    public boolean isOnDragOverEnable() {
        return onDragOverEnable;
    }

    public boolean isOnDragExitedEnable() {
        return onDragExitedEnable;
    }

    public void setOnDragEnteredEnable(boolean en) {
        onDragEnteredEnable = en;
    }

    public boolean isOnDragEnteredEnable() {
        return onDragEnteredEnable;
    }

    @Override
    public void setOnKeyTyped(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyTyped() == null) {
            super.setOnKeyTyped(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyTyped != null && onKeyTypedEnable) {
                        beforeOnKeyTyped(event);
                        onKeyTyped.handle(event);
                        afterOnKeyTyped(event);
                    }

                }
            });
        }
        if (onKeyTyped == null) {
            onKeyTyped = eh;
        } else {
            onKeyTyped = null;
            onKeyTyped = eh;
        }
    }

    @Override
    public void setOnKeyReleased(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyReleased() == null) {
            super.setOnKeyReleased(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyReleased != null && onKeyReleasedEnable) {
                        beforeOnKeyReleased(event);
                        onKeyReleased.handle(event);
                        afterOnKeyReleased(event);
                    }

                }
            });
        }
        if (onKeyReleased == null) {
            onKeyReleased = eh;
        } else {
            onKeyReleased = null;
            onKeyReleased = eh;
        }
    }

    @Override
    public void setOnKeyPressed(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyPressed() == null) {
            super.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyPressed != null && onKeyPressedEnable) {
                        beforeOnKeyPressed(event);
                        onKeyPressed.handle(event);
                        afterOnKeyPressed(event);
                    }

                }
            });
        }
        if (onKeyPressed == null) {
            onKeyPressed = eh;
        } else {
            onKeyPressed = null;
            onKeyPressed = eh;
        }
    }

    @Override
    public void setOnTouchStationary(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchStationary() == null) {
            super.setOnTouchStationary(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchStationary != null && onTouchStationaryEnable) {
                        beforeOnTouchStationary(event);
                        onTouchStationary.handle(event);
                        afterOnTouchStationary(event);
                    }

                }
            });
        }
        if (onTouchStationary == null) {
            onTouchStationary = eh;
        } else {
            onTouchStationary = null;
            onTouchStationary = eh;
        }
    }

    @Override
    public void setOnTouchReleased(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchReleased() == null) {
            super.setOnTouchReleased(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchReleased != null && onTouchReleasedEnable) {
                        beforeOnTouchReleased(event);
                        onTouchReleased.handle(event);
                        afterOnTouchReleased(event);
                    }

                }
            });
        }
        if (onTouchReleased == null) {
            onTouchReleased = eh;
        } else {
            onTouchReleased = null;
            onTouchReleased = eh;
        }
    }

    @Override
    public void setOnTouchMoved(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchMoved() == null) {
            super.setOnTouchMoved(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchMoved != null && onTouchMovedEnable) {
                        beforeOnTouchMoved(event);
                        onTouchMoved.handle(event);
                        afterOnTouchMoved(event);
                    }

                }
            });
        }
        if (onTouchMoved == null) {
            onTouchMoved = eh;
        } else {
            onTouchMoved = null;
            onTouchMoved = eh;
        }
    }

    @Override
    public void setOnTouchPressed(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchPressed() == null) {
            super.setOnTouchPressed(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchPressed != null && onTouchPressedEnable) {
                        beforeOnTouchPressed(event);
                        onTouchPressed.handle(event);
                        afterOnTouchPressed(event);
                    }

                }
            });
        }
        if (onTouchPressed == null) {
            onTouchPressed = eh;
        } else {
            onTouchPressed = null;
            onTouchPressed = eh;
        }
    }

    @Override
    public void setOnSwipeRight(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeRight() == null) {
            super.setOnSwipeRight(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeRight != null && onSwipeRightEnable) {
                        beforeOnSwipeRight(event);
                        onSwipeRight.handle(event);
                        afterOnSwipeRight(event);
                    }

                }
            });
        }
        if (onSwipeRight == null) {
            onSwipeRight = eh;
        } else {
            onSwipeRight = null;
            onSwipeRight = eh;
        }
    }

    @Override
    public void setOnSwipeLeft(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeLeft() == null) {
            super.setOnSwipeLeft(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeLeft != null && onSwipeLeftEnable) {
                        beforeOnSwipeLeft(event);
                        onSwipeLeft.handle(event);
                        afterOnSwipeLeft(event);
                    }

                }
            });
        }
        if (onSwipeLeft == null) {
            onSwipeLeft = eh;
        } else {
            onSwipeLeft = null;
            onSwipeLeft = eh;
        }
    }

    @Override
    public void setOnSwipeDown(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeDown() == null) {
            super.setOnSwipeDown(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeDown != null && onSwipeDownEnable) {
                        beforeOnSwipeDown(event);
                        onSwipeDown.handle(event);
                        afterOnSwipeDown(event);
                    }

                }
            });
        }
        if (onSwipeDown == null) {
            onSwipeDown = eh;
        } else {
            onSwipeDown = null;
            onSwipeDown = eh;
        }
    }

    @Override
    public void setOnSwipeUp(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeUp() == null) {
            super.setOnSwipeUp(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeUp != null && onSwipeUpEnable) {
                        beforeOnSwipeUp(event);
                        onSwipeUp.handle(event);
                        afterOnSwipeUp(event);
                    }

                }
            });
        }
        if (onSwipeUp == null) {
            onSwipeUp = eh;
        } else {
            onSwipeUp = null;
            onSwipeUp = eh;
        }
    }

    @Override
    public void setOnZoomFinished(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoomFinished() == null) {
            super.setOnZoomFinished(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoomFinished != null && onZoomFinishedEnable) {
                        beforeOnZoomFinished(event);
                        onZoomFinished.handle(event);
                        afterOnZoomFinished(event);
                    }

                }
            });
        }
        if (onZoomFinished == null) {
            onZoomFinished = eh;
        } else {
            onZoomFinished = null;
            onZoomFinished = eh;
        }
    }

    @Override
    public void setOnZoom(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoom() == null) {
            super.setOnZoom(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoom != null && onZoomEnable) {
                        beforeOnZoom(event);
                        onZoom.handle(event);
                        afterOnZoom(event);
                    }

                }
            });
        }
        if (onZoom == null) {
            onZoom = eh;
        } else {
            onZoom = null;
            onZoom = eh;
        }
    }

    @Override
    public void setOnZoomStarted(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoomStarted() == null) {
            super.setOnZoomStarted(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoomStarted != null && onZoomStartedEnable) {
                        beforeOnZoomStarted(event);
                        onZoomStarted.handle(event);
                        afterOnZoomStarted(event);
                    }

                }
            });
        }
        if (onZoomStarted == null) {
            onZoomStarted = eh;
        } else {
            onZoomStarted = null;
            onZoomStarted = eh;
        }
    }

    @Override
    public void setOnRotationFinished(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotationFinished() == null) {
            super.setOnRotationFinished(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotationFinished != null && onRotationFinishedEnable) {
                        beforeOnRotationFinished(event);
                        onRotationFinished.handle(event);
                        afterOnRotationFinished(event);
                    }

                }
            });
        }
        if (onRotationFinished == null) {
            onRotationFinished = eh;
        } else {
            onRotationFinished = null;
            onRotationFinished = eh;
        }
    }

    @Override
    public void setOnRotate(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotate() == null) {
            super.setOnRotate(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotate != null && onRotateEnable) {
                        beforeOnRotate(event);
                        onRotate.handle(event);
                        afterOnRotate(event);
                    }

                }
            });
        }
        if (onRotate == null) {
            onRotate = eh;
        } else {
            onRotate = null;
            onRotate = eh;
        }
    }

    @Override
    public void setOnRotationStarted(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotationStarted() == null) {
            super.setOnRotationStarted(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotationStarted != null && onRotationStartedEnable) {
                        beforeOnRotationStarted(event);
                        onRotationStarted.handle(event);
                        afterOnRotationStarted(event);
                    }

                }
            });
        }
        if (onRotationStarted == null) {
            onRotationStarted = eh;
        } else {
            onRotationStarted = null;
            onRotationStarted = eh;
        }
    }

    @Override
    public void setOnScrollFinished(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScrollFinished() == null) {
            super.setOnScrollFinished(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScrollFinished != null && onScrollFinishedEnable) {
                        beforeOnScrollFinished(event);
                        onScrollFinished.handle(event);
                        afterOnScrollFinished(event);
                    }

                }
            });
        }
        if (onScrollFinished == null) {
            onScrollFinished = eh;
        } else {
            onScrollFinished = null;
            onScrollFinished = eh;
        }
    }

    @Override
    public void setOnScroll(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScroll() == null) {
            super.setOnScroll(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScroll != null && onScrollEnable) {
                        beforeOnScroll(event);
                        onScroll.handle(event);
                        afterOnScroll(event);
                    }

                }
            });
        }
        if (onScroll == null) {
            onScroll = eh;
        } else {
            onScroll = null;
            onScroll = eh;
        }
    }

    @Override
    public void setOnScrollStarted(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScrollStarted() == null) {
            super.setOnScrollStarted(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScrollStarted != null && onScrollStartedEnable) {
                        beforeOnScrollStarted(event);
                        onScrollStarted.handle(event);
                        afterOnScrollStarted(event);
                    }

                }
            });
        }
        if (onScrollStarted == null) {
            onScrollStarted = eh;
        } else {
            onScrollStarted = null;
            onScrollStarted = eh;
        }
    }

    @Override
    public void setOnMouseDragExited(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragExited() == null) {
            super.setOnMouseDragExited(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragExited != null && onMouseDragExitedEnable) {
                        beforeOnMouseDragExited(event);
                        onMouseDragExited.handle(event);
                        afterOnMouseDragExited(event);
                    }

                }
            });
        }
        if (onMouseDragExited == null) {
            onMouseDragExited = eh;
        } else {
            onMouseDragExited = null;
            onMouseDragExited = eh;
        }
    }

    @Override
    public void setOnMouseDragEntered(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragEntered() == null) {
            super.setOnMouseDragEntered(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragEntered != null && onMouseDragEnteredEnable) {
                        beforeOnMouseDragEntered(event);
                        onMouseDragEntered.handle(event);
                        afterOnMouseDragEntered(event);
                    }

                }
            });
        }
        if (onMouseDragEntered == null) {
            onMouseDragEntered = eh;
        } else {
            onMouseDragEntered = null;
            onMouseDragEntered = eh;
        }
    }

    @Override
    public void setOnMouseDragReleased(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragReleased() == null) {
            super.setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragReleased != null && onMouseDragReleasedEnable) {
                        beforeOnMouseDragReleased(event);
                        onMouseDragReleased.handle(event);
                        afterOnMouseDragReleased(event);
                    }

                }
            });
        }
        if (onMouseDragReleased == null) {
            onMouseDragReleased = eh;
        } else {
            onMouseDragReleased = null;
            onMouseDragReleased = eh;
        }
    }

    @Override
    public void setOnMouseDragOver(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragOver() == null) {
            super.setOnMouseDragOver(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragOver != null && onMouseDragOverEnable) {
                        beforeOnMouseDragOver(event);
                        onMouseDragOver.handle(event);
                        afterOnMouseDragOver(event);
                    }

                }
            });
        }
        if (onMouseDragOver == null) {
            onMouseDragOver = eh;
        } else {
            onMouseDragOver = null;
            onMouseDragOver = eh;
        }
    }

    @Override
    public void setOnDragDetected(EventHandler<? super MouseEvent> eh) {

        if (super.getOnDragDetected() == null) {
            super.setOnDragDetected(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onDragDetected != null && onDragDetectedEnable) {
                        beforeOnDragDetected(event);
                        onDragDetected.handle(event);
                        afterOnDragDetected(event);
                    }

                }
            });
        }
        if (onDragDetected == null) {
            onDragDetected = eh;
        } else {
            onDragDetected = null;
            onDragDetected = eh;
        }
    }

    @Override
    public void setOnMouseReleased(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseReleased() == null) {
            super.setOnMouseReleased(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseReleased != null && onMouseReleasedEnable) {
                        beforeOnMouseReleased(event);
                        onMouseReleased.handle(event);
                        afterOnMouseReleased(event);
                    }

                }
            });
        }
        if (onMouseReleased == null) {
            onMouseReleased = eh;
        } else {
            onMouseReleased = null;
            onMouseReleased = eh;
        }
    }

    @Override
    public void setOnMousePressed(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMousePressed() == null) {
            super.setOnMousePressed(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMousePressed != null && onMousePressedEnable) {
                        beforeOnMousePressed(event);
                        onMousePressed.handle(event);
                        afterOnMousePressed(event);
                    }

                }
            });
        }
        if (onMousePressed == null) {
            onMousePressed = eh;
        } else {
            onMousePressed = null;
            onMousePressed = eh;
        }
    }

    @Override
    public void setOnMouseMoved(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseMoved() == null) {
            super.setOnMouseMoved(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseMoved != null && onMouseMovedEnable) {
                        beforeOnMouseMoved(event);
                        onMouseMoved.handle(event);
                        afterOnMouseMoved(event);
                    }

                }
            });
        }
        if (onMouseMoved == null) {
            onMouseMoved = eh;
        } else {
            onMouseMoved = null;
            onMouseMoved = eh;
        }
    }

    @Override
    public void setOnMouseExited(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseExited() == null) {
            super.setOnMouseExited(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseExited != null && onMouseExitedEnable) {
                        beforeOnMouseExited(event);
                        onMouseExited.handle(event);
                        afterOnMouseExited(event);
                    }

                }
            });
        }
        if (onMouseExited == null) {
            onMouseExited = eh;
        } else {
            onMouseExited = null;
            onMouseExited = eh;
        }
    }

    @Override
    public void setOnMouseEntered(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseEntered() == null) {
            super.setOnMouseEntered(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseEntered != null && onMouseEnteredEnable) {
                        beforeOnMouseEntered(event);
                        onMouseEntered.handle(event);
                        afterOnMouseEntered(event);
                    }

                }
            });
        }
        if (onMouseEntered == null) {
            onMouseEntered = eh;
        } else {
            onMouseEntered = null;
            onMouseEntered = eh;
        }
    }

    @Override
    public void setOnMouseDragged(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseDragged() == null) {
            super.setOnMouseDragged(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseDragged != null && onMouseDraggedEnable) {
                        beforeOnMouseDragged(event);
                        onMouseDragged.handle(event);
                        afterOnMouseDragged(event);
                    }

                }
            });
        }
        if (onMouseDragged == null) {
            onMouseDragged = eh;
        } else {
            onMouseDragged = null;
            onMouseDragged = eh;
        }
    }

    @Override
    public void setOnMouseClicked(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseClicked() == null) {
            super.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseClicked != null && onMouseClickedEnable) {
                        beforeOnMouseClicked(event);
                        onMouseClicked.handle(event);
                        afterOnMouseClicked(event);
                    }

                }
            });
        }
        if (onMouseClicked == null) {
            onMouseClicked = eh;
        } else {
            onMouseClicked = null;
            onMouseClicked = eh;
        }
    }

    @Override
    public void setOnDragDone(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragDone() == null) {
            super.setOnDragDone(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragDone != null && onDragDoneEnable) {
                        beforeOnDragDone(event);
                        onDragDone.handle(event);
                        afterOnDragDone(event);
                    }

                }
            });
        }
        if (onDragDone == null) {
            onDragDone = eh;
        } else {
            onDragDone = null;
            onDragDone = eh;
        }
    }

    @Override
    public void setOnDragDropped(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragDropped() == null) {
            super.setOnDragDropped(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragDropped != null && onDragDroppedEnable) {
                        beforeOnDragDropped(event);
                        onDragDropped.handle(event);
                        afterOnDragDropped(event);
                    }

                }
            });
        }
        if (onDragDropped == null) {
            onDragDropped = eh;
        } else {
            onDragDropped = null;
            onDragDropped = eh;
        }
    }

    @Override
    public void setOnDragOver(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragOver() == null) {
            super.setOnDragOver(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragOver != null && onDragOverEnable) {
                        beforeOnDragOver(event);
                        onDragOver.handle(event);
                        afterOnDragOver(event);
                    }

                }
            });
        }
        if (onDragOver == null) {
            onDragOver = eh;
        } else {
            onDragOver = null;
            onDragOver = eh;
        }
    }

    @Override
    public void setOnDragExited(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragExited() == null) {
            super.setOnDragExited(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragExited != null && onDragExitedEnable) {
                        beforeOnDragExited(event);
                        onDragExited.handle(event);
                        afterOnDragExited(event);
                    }

                }
            });
        }
        if (onDragExited == null) {
            onDragExited = eh;
        } else {
            onDragExited = null;
            onDragExited = eh;
        }
    }

    @Override
    public void setOnDragEntered(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragEntered() == null) {
            super.setOnDragEntered(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragEntered != null && onDragEnteredEnable) {
                        beforeOnDragEntered(event);
                        onDragEntered.handle(event);
                        afterOnDragEntered(event);
                    }

                }
            });
        }
        if (onDragEntered == null) {
            onDragEntered = eh;
        } else {
            onDragEntered = null;
            onDragEntered = eh;
        }
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyTyped() {
        return onKeyTyped;
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyReleased() {
        return onKeyReleased;
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyPressed() {
        return onKeyPressed;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchStationary() {
        return onTouchStationary;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchReleased() {
        return onTouchReleased;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchMoved() {
        return onTouchMoved;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchPressed() {
        return onTouchPressed;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeRight() {
        return onSwipeRight;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeLeft() {
        return onSwipeLeft;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeDown() {
        return onSwipeDown;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeUp() {
        return onSwipeUp;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoomFinished() {
        return onZoomFinished;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoom() {
        return onZoom;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoomStarted() {
        return onZoomStarted;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotationFinished() {
        return onRotationFinished;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotate() {
        return onRotate;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotationStarted() {
        return onRotationStarted;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScrollFinished() {
        return onScrollFinished;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScroll() {
        return onScroll;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScrollStarted() {
        return onScrollStarted;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragExited() {
        return onMouseDragExited;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragEntered() {
        return onMouseDragEntered;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragReleased() {
        return onMouseDragReleased;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragOver() {
        return onMouseDragOver;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnDragDetected() {
        return onDragDetected;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseReleased() {
        return onMouseReleased;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMousePressed() {
        return onMousePressed;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseMoved() {
        return onMouseMoved;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseExited() {
        return onMouseExited;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseEntered() {
        return onMouseEntered;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseDragged() {
        return onMouseDragged;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseClicked() {
        return onMouseClicked;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragDone() {
        return onDragDone;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragDropped() {
        return onDragDropped;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragOver() {
        return onDragOver;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragExited() {
        return onDragExited;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragEntered() {
        return onDragEntered;
    }

    protected void beforeOnKeyTyped(KeyEvent event) {
    }

    protected void afterOnKeyTyped(KeyEvent eve) {
    }

    protected void beforeOnKeyReleased(KeyEvent event) {
    }

    protected void afterOnKeyReleased(KeyEvent eve) {
    }

    protected void beforeOnKeyPressed(KeyEvent event) {
    }

    protected void afterOnKeyPressed(KeyEvent eve) {
    }

    protected void beforeOnTouchStationary(TouchEvent event) {
    }

    protected void afterOnTouchStationary(TouchEvent eve) {
    }

    protected void beforeOnTouchReleased(TouchEvent event) {
    }

    protected void afterOnTouchReleased(TouchEvent eve) {
    }

    protected void beforeOnTouchMoved(TouchEvent event) {
    }

    protected void afterOnTouchMoved(TouchEvent eve) {
    }

    protected void beforeOnTouchPressed(TouchEvent event) {
    }

    protected void afterOnTouchPressed(TouchEvent eve) {
    }

    protected void beforeOnSwipeRight(SwipeEvent event) {
    }

    protected void afterOnSwipeRight(SwipeEvent eve) {
    }

    protected void beforeOnSwipeLeft(SwipeEvent event) {
    }

    protected void afterOnSwipeLeft(SwipeEvent eve) {
    }

    protected void beforeOnSwipeDown(SwipeEvent event) {
    }

    protected void afterOnSwipeDown(SwipeEvent eve) {
    }

    protected void beforeOnSwipeUp(SwipeEvent event) {
    }

    protected void afterOnSwipeUp(SwipeEvent eve) {
    }

    protected void beforeOnZoomFinished(ZoomEvent event) {
    }

    protected void afterOnZoomFinished(ZoomEvent eve) {
    }

    protected void beforeOnZoom(ZoomEvent event) {
    }

    protected void afterOnZoom(ZoomEvent eve) {
    }

    protected void beforeOnZoomStarted(ZoomEvent event) {
    }

    protected void afterOnZoomStarted(ZoomEvent eve) {
    }

    protected void beforeOnRotationFinished(RotateEvent event) {
    }

    protected void afterOnRotationFinished(RotateEvent eve) {
    }

    protected void beforeOnRotate(RotateEvent event) {
    }

    protected void afterOnRotate(RotateEvent eve) {
    }

    protected void beforeOnRotationStarted(RotateEvent event) {
    }

    protected void afterOnRotationStarted(RotateEvent eve) {
    }

    protected void beforeOnScrollFinished(ScrollEvent event) {
    }

    protected void afterOnScrollFinished(ScrollEvent eve) {
    }

    protected void beforeOnScroll(ScrollEvent event) {
    }

    protected void afterOnScroll(ScrollEvent eve) {
    }

    protected void beforeOnScrollStarted(ScrollEvent event) {
    }

    protected void afterOnScrollStarted(ScrollEvent eve) {
    }

    protected void beforeOnMouseDragExited(MouseDragEvent event) {
    }

    protected void afterOnMouseDragExited(MouseDragEvent eve) {
    }

    protected void beforeOnMouseDragEntered(MouseDragEvent event) {
    }

    protected void afterOnMouseDragEntered(MouseDragEvent eve) {
    }

    protected void beforeOnMouseDragReleased(MouseDragEvent event) {
    }

    protected void afterOnMouseDragReleased(MouseDragEvent eve) {
    }

    protected void beforeOnMouseDragOver(MouseDragEvent event) {
    }

    protected void afterOnMouseDragOver(MouseDragEvent eve) {
    }

    protected void beforeOnDragDetected(MouseEvent event) {
    }

    protected void afterOnDragDetected(MouseEvent eve) {
    }

    protected void beforeOnMouseReleased(MouseEvent event) {
    }

    protected void afterOnMouseReleased(MouseEvent eve) {
    }

    protected void beforeOnMousePressed(MouseEvent event) {
    }

    protected void afterOnMousePressed(MouseEvent eve) {
    }

    protected void beforeOnMouseMoved(MouseEvent event) {
    }

    protected void afterOnMouseMoved(MouseEvent eve) {
    }

    protected void beforeOnMouseExited(MouseEvent event) {
    }

    protected void afterOnMouseExited(MouseEvent eve) {
    }

    protected void beforeOnMouseEntered(MouseEvent event) {
    }

    protected void afterOnMouseEntered(MouseEvent eve) {
    }

    protected void beforeOnMouseDragged(MouseEvent event) {
    }

    protected void afterOnMouseDragged(MouseEvent eve) {
    }

    protected void beforeOnMouseClicked(MouseEvent event) {
    }

    protected void afterOnMouseClicked(MouseEvent eve) {
    }

    protected void beforeOnDragDone(DragEvent event) {
    }

    protected void afterOnDragDone(DragEvent eve) {
    }

    protected void beforeOnDragDropped(DragEvent event) {
    }

    protected void afterOnDragDropped(DragEvent eve) {
    }

    protected void beforeOnDragOver(DragEvent event) {
    }

    protected void afterOnDragOver(DragEvent eve) {
    }

    protected void beforeOnDragExited(DragEvent event) {
    }

    protected void afterOnDragExited(DragEvent eve) {
    }

    protected void beforeOnDragEntered(DragEvent event) {
    }

    protected void afterOnDragEntered(DragEvent eve) {
    }
    
    
    
    public void sowp(Node n,int j){
       
        int i=getChildren().indexOf(n);
        j=i+j;
        
       
        if(i<getChildren().size() && j<getChildren().size() && i>-1 && j>-1){
            Node n1=getChildren().get(i);
            Node n2=getChildren().get(j);

            Group q=new Group();
            getChildren().set(i, q);
            getChildren().set(j, n1);
            getChildren().set(i, n2);
        }
    }

}
