/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.container;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.ZoomEvent;

public class MyAnchorPaneCover extends MyAnchorPane {

    private EventHandler<? super KeyEvent> onKeyTyped;
    private EventHandler<? super KeyEvent> onKeyReleased;
    private EventHandler<? super KeyEvent> onKeyPressed;
    private EventHandler<? super TouchEvent> onTouchStationary;
    private EventHandler<? super TouchEvent> onTouchReleased;
    private EventHandler<? super TouchEvent> onTouchMoved;
    private EventHandler<? super TouchEvent> onTouchPressed;
    private EventHandler<? super SwipeEvent> onSwipeRight;
    private EventHandler<? super SwipeEvent> onSwipeLeft;
    private EventHandler<? super SwipeEvent> onSwipeDown;
    private EventHandler<? super SwipeEvent> onSwipeUp;
    private EventHandler<? super ZoomEvent> onZoomFinished;
    private EventHandler<? super ZoomEvent> onZoom;
    private EventHandler<? super ZoomEvent> onZoomStarted;
    private EventHandler<? super RotateEvent> onRotationFinished;
    private EventHandler<? super RotateEvent> onRotate;
    private EventHandler<? super RotateEvent> onRotationStarted;
    private EventHandler<? super ScrollEvent> onScrollFinished;
    private EventHandler<? super ScrollEvent> onScroll;
    private EventHandler<? super ScrollEvent> onScrollStarted;
    private EventHandler<? super MouseDragEvent> onMouseDragExited;
    private EventHandler<? super MouseDragEvent> onMouseDragEntered;
    private EventHandler<? super MouseDragEvent> onMouseDragReleased;
    private EventHandler<? super MouseDragEvent> onMouseDragOver;
    private EventHandler<? super MouseEvent> onDragDetected;
    private EventHandler<? super MouseEvent> onMouseReleased;
    private EventHandler<? super MouseEvent> onMousePressed;
    private EventHandler<? super MouseEvent> onMouseMoved;
    private EventHandler<? super MouseEvent> onMouseExited;
    private EventHandler<? super MouseEvent> onMouseEntered;
    private EventHandler<? super MouseEvent> onMouseDragged;
    private EventHandler<? super MouseEvent> onMouseClicked;
    private EventHandler<? super DragEvent> onDragDone;
    private EventHandler<? super DragEvent> onDragDropped;
    private EventHandler<? super DragEvent> onDragOver;
    private EventHandler<? super DragEvent> onDragExited;
    private EventHandler<? super DragEvent> onDragEntered;

    private boolean onKeyTypedEnable = true;
    private boolean onKeyReleasedEnable = true;
    private boolean onKeyPressedEnable = true;
    private boolean onTouchStationaryEnable = true;
    private boolean onTouchReleasedEnable = true;
    private boolean onTouchMovedEnable = true;
    private boolean onTouchPressedEnable = true;
    private boolean onSwipeRightEnable = true;
    private boolean onSwipeLeftEnable = true;
    private boolean onSwipeDownEnable = true;
    private boolean onSwipeUpEnable = true;
    private boolean onZoomFinishedEnable = true;
    private boolean onZoomEnable = true;
    private boolean onZoomStartedEnable = true;
    private boolean onRotationFinishedEnable = true;
    private boolean onRotateEnable = true;
    private boolean onRotationStartedEnable = true;
    private boolean onScrollFinishedEnable = true;
    private boolean onScrollEnable = true;
    private boolean onScrollStartedEnable = true;
    private boolean onMouseDragExitedEnable = true;
    private boolean onMouseDragEnteredEnable = true;
    private boolean onMouseDragReleasedEnable = true;
    private boolean onMouseDragOverEnable = true;
    private boolean onDragDetectedEnable = true;
    private boolean onMouseReleasedEnable = true;
    private boolean onMousePressedEnable = true;
    private boolean onMouseMovedEnable = true;
    private boolean onMouseExitedEnable = true;
    private boolean onMouseEnteredEnable = true;
    private boolean onMouseDraggedEnable = true;
    private boolean onMouseClickedEnable = true;
    private boolean onDragDoneEnable = true;
    private boolean onDragDroppedEnable = true;
    private boolean onDragOverEnable = true;
    private boolean onDragExitedEnable = true;
    private boolean onDragEnteredEnable = true;

    protected void setOnKeyTypedEnable(boolean en) {
        onKeyTypedEnable = en;
    }

    protected void setOnKeyReleasedEnable(boolean en) {
        onKeyReleasedEnable = en;
    }

    protected void setOnKeyPressedEnable(boolean en) {
        onKeyPressedEnable = en;
    }

    protected void setOnTouchStationaryEnable(boolean en) {
        onTouchStationaryEnable = en;
    }

    protected void setOnTouchReleasedEnable(boolean en) {
        onTouchReleasedEnable = en;
    }

    protected void setOnTouchMovedEnable(boolean en) {
        onTouchMovedEnable = en;
    }

    protected void setOnTouchPressedEnable(boolean en) {
        onTouchPressedEnable = en;
    }

    protected void setOnSwipeRightEnable(boolean en) {
        onSwipeRightEnable = en;
    }

    protected void setOnSwipeLeftEnable(boolean en) {
        onSwipeLeftEnable = en;
    }

    protected void setOnSwipeDownEnable(boolean en) {
        onSwipeDownEnable = en;
    }

    protected void setOnSwipeUpEnable(boolean en) {
        onSwipeUpEnable = en;
    }

    protected void setOnZoomFinishedEnable(boolean en) {
        onZoomFinishedEnable = en;
    }

    protected void setOnZoomEnable(boolean en) {
        onZoomEnable = en;
    }

    protected void setOnZoomStartedEnable(boolean en) {
        onZoomStartedEnable = en;
    }

    protected void setOnRotationFinishedEnable(boolean en) {
        onRotationFinishedEnable = en;
    }

    protected void setOnRotateEnable(boolean en) {
        onRotateEnable = en;
    }

    protected void setOnRotationStartedEnable(boolean en) {
        onRotationStartedEnable = en;
    }

    protected void setOnScrollFinishedEnable(boolean en) {
        onScrollFinishedEnable = en;
    }

    protected void setOnScrollEnable(boolean en) {
        onScrollEnable = en;
    }

    protected void setOnScrollStartedEnable(boolean en) {
        onScrollStartedEnable = en;
    }

    protected void setOnMouseDragExitedEnable(boolean en) {
        onMouseDragExitedEnable = en;
    }

    protected void setOnMouseDragEnteredEnable(boolean en) {
        onMouseDragEnteredEnable = en;
    }

    protected void setOnMouseDragReleasedEnable(boolean en) {
        onMouseDragReleasedEnable = en;
    }

    protected void setOnMouseDragOverEnable(boolean en) {
        onMouseDragOverEnable = en;
    }

    protected void setOnDragDetectedEnable(boolean en) {
        onDragDetectedEnable = en;
    }

    protected void setOnMouseReleasedEnable(boolean en) {
        onMouseReleasedEnable = en;
    }

    protected void setOnMousePressedEnable(boolean en) {
        onMousePressedEnable = en;
    }

    protected void setOnMouseMovedEnable(boolean en) {
        onMouseMovedEnable = en;
    }

    protected void setOnMouseExitedEnable(boolean en) {
        onMouseExitedEnable = en;
    }

    protected void setOnMouseEnteredEnable(boolean en) {
        onMouseEnteredEnable = en;
    }

    protected void setOnMouseDraggedEnable(boolean en) {
        onMouseDraggedEnable = en;
    }

    protected void setOnMouseClickedEnable(boolean en) {
        onMouseClickedEnable = en;
    }

    protected void setOnDragDoneEnable(boolean en) {
        onDragDoneEnable = en;
    }

    protected void setOnDragDroppedEnable(boolean en) {
        onDragDroppedEnable = en;
    }

    protected void setOnDragOverEnable(boolean en) {
        onDragOverEnable = en;
    }

    protected void setOnDragExitedEnable(boolean en) {
        onDragExitedEnable = en;
    }

    protected boolean isOnKeyTypedEnable() {
        return onKeyTypedEnable;
    }

    protected boolean isOnKeyReleasedEnable() {
        return onKeyReleasedEnable;
    }

    protected boolean isOnKeyPressedEnable() {
        return onKeyPressedEnable;
    }

    protected boolean isOnTouchStationaryEnable() {
        return onTouchStationaryEnable;
    }

    protected boolean isOnTouchReleasedEnable() {
        return onTouchReleasedEnable;
    }

    protected boolean isOnTouchMovedEnable() {
        return onTouchMovedEnable;
    }

    protected boolean isOnTouchPressedEnable() {
        return onTouchPressedEnable;
    }

    protected boolean isOnSwipeRightEnable() {
        return onSwipeRightEnable;
    }

    protected boolean isOnSwipeLeftEnable() {
        return onSwipeLeftEnable;
    }

    protected boolean isOnSwipeDownEnable() {
        return onSwipeDownEnable;
    }

    protected boolean isOnSwipeUpEnable() {
        return onSwipeUpEnable;
    }

    protected boolean isOnZoomFinishedEnable() {
        return onZoomFinishedEnable;
    }

    protected boolean isOnZoomEnable() {
        return onZoomEnable;
    }

    protected boolean isOnZoomStartedEnable() {
        return onZoomStartedEnable;
    }

    protected boolean isOnRotationFinishedEnable() {
        return onRotationFinishedEnable;
    }

    protected boolean isOnRotateEnable() {
        return onRotateEnable;
    }

    protected boolean isOnRotationStartedEnable() {
        return onRotationStartedEnable;
    }

    protected boolean isOnScrollFinishedEnable() {
        return onScrollFinishedEnable;
    }

    protected boolean isOnScrollEnable() {
        return onScrollEnable;
    }

    protected boolean isOnScrollStartedEnable() {
        return onScrollStartedEnable;
    }

    protected boolean isOnMouseDragExitedEnable() {
        return onMouseDragExitedEnable;
    }

    protected boolean isOnMouseDragEnteredEnable() {
        return onMouseDragEnteredEnable;
    }

    protected boolean isOnMouseDragReleasedEnable() {
        return onMouseDragReleasedEnable;
    }

    protected boolean isOnMouseDragOverEnable() {
        return onMouseDragOverEnable;
    }

    protected boolean isOnDragDetectedEnable() {
        return onDragDetectedEnable;
    }

    protected boolean isOnMouseReleasedEnable() {
        return onMouseReleasedEnable;
    }

    protected boolean isOnMousePressedEnable() {
        return onMousePressedEnable;
    }

    protected boolean isOnMouseMovedEnable() {
        return onMouseMovedEnable;
    }

    protected boolean isOnMouseExitedEnable() {
        return onMouseExitedEnable;
    }

    protected boolean isOnMouseEnteredEnable() {
        return onMouseEnteredEnable;
    }

    protected boolean isOnMouseDraggedEnable() {
        return onMouseDraggedEnable;
    }

    protected boolean isOnMouseClickedEnable() {
        return onMouseClickedEnable;
    }

    protected boolean isOnDragDoneEnable() {
        return onDragDoneEnable;
    }

    protected boolean isOnDragDroppedEnable() {
        return onDragDroppedEnable;
    }

    protected boolean isOnDragOverEnable() {
        return onDragOverEnable;
    }

    protected boolean isOnDragExitedEnable() {
        return onDragExitedEnable;
    }

    protected void setOnDragEnteredEnable(boolean en) {
        onDragEnteredEnable = en;
    }

    protected boolean isOnDragEnteredEnable() {
        return onDragEnteredEnable;
    }

    @Override
    public void setOnKeyTyped(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyTyped() == null) {
            super.setOnKeyTyped(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyTyped != null && onKeyTypedEnable) {
                        beforeOnKeyTyped();
                        onKeyTyped.handle(event);
                        afterOnKeyTyped();
                    }

                }
            });
        }
        if (onKeyTyped == null) {
            onKeyTyped = eh;
        } else {
            onKeyTyped = null;
            onKeyTyped = eh;
        }
    }

    @Override
    public void setOnKeyReleased(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyReleased() == null) {
            super.setOnKeyReleased(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyReleased != null && onKeyReleasedEnable) {
                        beforeOnKeyReleased();
                        onKeyReleased.handle(event);
                        afterOnKeyReleased();
                    }

                }
            });
        }
        if (onKeyReleased == null) {
            onKeyReleased = eh;
        } else {
            onKeyReleased = null;
            onKeyReleased = eh;
        }
    }

    @Override
    public void setOnKeyPressed(EventHandler<? super KeyEvent> eh) {

        if (super.getOnKeyPressed() == null) {
            super.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent event) {

                    if (onKeyPressed != null && onKeyPressedEnable) {
                        beforeOnKeyPressed();
                        onKeyPressed.handle(event);
                        afterOnKeyPressed();
                    }

                }
            });
        }
        if (onKeyPressed == null) {
            onKeyPressed = eh;
        } else {
            onKeyPressed = null;
            onKeyPressed = eh;
        }
    }

    @Override
    public void setOnTouchStationary(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchStationary() == null) {
            super.setOnTouchStationary(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchStationary != null && onTouchStationaryEnable) {
                        beforeOnTouchStationary();
                        onTouchStationary.handle(event);
                        afterOnTouchStationary();
                    }

                }
            });
        }
        if (onTouchStationary == null) {
            onTouchStationary = eh;
        } else {
            onTouchStationary = null;
            onTouchStationary = eh;
        }
    }

    @Override
    public void setOnTouchReleased(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchReleased() == null) {
            super.setOnTouchReleased(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchReleased != null && onTouchReleasedEnable) {
                        beforeOnTouchReleased();
                        onTouchReleased.handle(event);
                        afterOnTouchReleased();
                    }

                }
            });
        }
        if (onTouchReleased == null) {
            onTouchReleased = eh;
        } else {
            onTouchReleased = null;
            onTouchReleased = eh;
        }
    }

    @Override
    public void setOnTouchMoved(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchMoved() == null) {
            super.setOnTouchMoved(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchMoved != null && onTouchMovedEnable) {
                        beforeOnTouchMoved();
                        onTouchMoved.handle(event);
                        afterOnTouchMoved();
                    }

                }
            });
        }
        if (onTouchMoved == null) {
            onTouchMoved = eh;
        } else {
            onTouchMoved = null;
            onTouchMoved = eh;
        }
    }

    @Override
    public void setOnTouchPressed(EventHandler<? super TouchEvent> eh) {

        if (super.getOnTouchPressed() == null) {
            super.setOnTouchPressed(new EventHandler<TouchEvent>() {

                @Override
                public void handle(TouchEvent event) {

                    if (onTouchPressed != null && onTouchPressedEnable) {
                        beforeOnTouchPressed();
                        onTouchPressed.handle(event);
                        afterOnTouchPressed();
                    }

                }
            });
        }
        if (onTouchPressed == null) {
            onTouchPressed = eh;
        } else {
            onTouchPressed = null;
            onTouchPressed = eh;
        }
    }

    @Override
    public void setOnSwipeRight(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeRight() == null) {
            super.setOnSwipeRight(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeRight != null && onSwipeRightEnable) {
                        beforeOnSwipeRight();
                        onSwipeRight.handle(event);
                        afterOnSwipeRight();
                    }

                }
            });
        }
        if (onSwipeRight == null) {
            onSwipeRight = eh;
        } else {
            onSwipeRight = null;
            onSwipeRight = eh;
        }
    }

    @Override
    public void setOnSwipeLeft(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeLeft() == null) {
            super.setOnSwipeLeft(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeLeft != null && onSwipeLeftEnable) {
                        beforeOnSwipeLeft();
                        onSwipeLeft.handle(event);
                        afterOnSwipeLeft();
                    }

                }
            });
        }
        if (onSwipeLeft == null) {
            onSwipeLeft = eh;
        } else {
            onSwipeLeft = null;
            onSwipeLeft = eh;
        }
    }

    @Override
    public void setOnSwipeDown(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeDown() == null) {
            super.setOnSwipeDown(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeDown != null && onSwipeDownEnable) {
                        beforeOnSwipeDown();
                        onSwipeDown.handle(event);
                        afterOnSwipeDown();
                    }

                }
            });
        }
        if (onSwipeDown == null) {
            onSwipeDown = eh;
        } else {
            onSwipeDown = null;
            onSwipeDown = eh;
        }
    }

    @Override
    public void setOnSwipeUp(EventHandler<? super SwipeEvent> eh) {

        if (super.getOnSwipeUp() == null) {
            super.setOnSwipeUp(new EventHandler<SwipeEvent>() {

                @Override
                public void handle(SwipeEvent event) {

                    if (onSwipeUp != null && onSwipeUpEnable) {
                        beforeOnSwipeUp();
                        onSwipeUp.handle(event);
                        afterOnSwipeUp();
                    }

                }
            });
        }
        if (onSwipeUp == null) {
            onSwipeUp = eh;
        } else {
            onSwipeUp = null;
            onSwipeUp = eh;
        }
    }

    @Override
    public void setOnZoomFinished(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoomFinished() == null) {
            super.setOnZoomFinished(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoomFinished != null && onZoomFinishedEnable) {
                        beforeOnZoomFinished();
                        onZoomFinished.handle(event);
                        afterOnZoomFinished();
                    }

                }
            });
        }
        if (onZoomFinished == null) {
            onZoomFinished = eh;
        } else {
            onZoomFinished = null;
            onZoomFinished = eh;
        }
    }

    @Override
    public void setOnZoom(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoom() == null) {
            super.setOnZoom(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoom != null && onZoomEnable) {
                        beforeOnZoom();
                        onZoom.handle(event);
                        afterOnZoom();
                    }

                }
            });
        }
        if (onZoom == null) {
            onZoom = eh;
        } else {
            onZoom = null;
            onZoom = eh;
        }
    }

    @Override
    public void setOnZoomStarted(EventHandler<? super ZoomEvent> eh) {

        if (super.getOnZoomStarted() == null) {
            super.setOnZoomStarted(new EventHandler<ZoomEvent>() {

                @Override
                public void handle(ZoomEvent event) {

                    if (onZoomStarted != null && onZoomStartedEnable) {
                        beforeOnZoomStarted();
                        onZoomStarted.handle(event);
                        afterOnZoomStarted();
                    }

                }
            });
        }
        if (onZoomStarted == null) {
            onZoomStarted = eh;
        } else {
            onZoomStarted = null;
            onZoomStarted = eh;
        }
    }

    @Override
    public void setOnRotationFinished(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotationFinished() == null) {
            super.setOnRotationFinished(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotationFinished != null && onRotationFinishedEnable) {
                        beforeOnRotationFinished();
                        onRotationFinished.handle(event);
                        afterOnRotationFinished();
                    }

                }
            });
        }
        if (onRotationFinished == null) {
            onRotationFinished = eh;
        } else {
            onRotationFinished = null;
            onRotationFinished = eh;
        }
    }

    @Override
    public void setOnRotate(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotate() == null) {
            super.setOnRotate(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotate != null && onRotateEnable) {
                        beforeOnRotate();
                        onRotate.handle(event);
                        afterOnRotate();
                    }

                }
            });
        }
        if (onRotate == null) {
            onRotate = eh;
        } else {
            onRotate = null;
            onRotate = eh;
        }
    }

    @Override
    public void setOnRotationStarted(EventHandler<? super RotateEvent> eh) {

        if (super.getOnRotationStarted() == null) {
            super.setOnRotationStarted(new EventHandler<RotateEvent>() {

                @Override
                public void handle(RotateEvent event) {

                    if (onRotationStarted != null && onRotationStartedEnable) {
                        beforeOnRotationStarted();
                        onRotationStarted.handle(event);
                        afterOnRotationStarted();
                    }

                }
            });
        }
        if (onRotationStarted == null) {
            onRotationStarted = eh;
        } else {
            onRotationStarted = null;
            onRotationStarted = eh;
        }
    }

    @Override
    public void setOnScrollFinished(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScrollFinished() == null) {
            super.setOnScrollFinished(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScrollFinished != null && onScrollFinishedEnable) {
                        beforeOnScrollFinished();
                        onScrollFinished.handle(event);
                        afterOnScrollFinished();
                    }

                }
            });
        }
        if (onScrollFinished == null) {
            onScrollFinished = eh;
        } else {
            onScrollFinished = null;
            onScrollFinished = eh;
        }
    }

    @Override
    public void setOnScroll(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScroll() == null) {
            super.setOnScroll(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScroll != null && onScrollEnable) {
                        beforeOnScroll();
                        onScroll.handle(event);
                        afterOnScroll();
                    }

                }
            });
        }
        if (onScroll == null) {
            onScroll = eh;
        } else {
            onScroll = null;
            onScroll = eh;
        }
    }

    @Override
    public void setOnScrollStarted(EventHandler<? super ScrollEvent> eh) {

        if (super.getOnScrollStarted() == null) {
            super.setOnScrollStarted(new EventHandler<ScrollEvent>() {

                @Override
                public void handle(ScrollEvent event) {

                    if (onScrollStarted != null && onScrollStartedEnable) {
                        beforeOnScrollStarted();
                        onScrollStarted.handle(event);
                        afterOnScrollStarted();
                    }

                }
            });
        }
        if (onScrollStarted == null) {
            onScrollStarted = eh;
        } else {
            onScrollStarted = null;
            onScrollStarted = eh;
        }
    }

    @Override
    public void setOnMouseDragExited(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragExited() == null) {
            super.setOnMouseDragExited(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragExited != null && onMouseDragExitedEnable) {
                        beforeOnMouseDragExited();
                        onMouseDragExited.handle(event);
                        afterOnMouseDragExited();
                    }

                }
            });
        }
        if (onMouseDragExited == null) {
            onMouseDragExited = eh;
        } else {
            onMouseDragExited = null;
            onMouseDragExited = eh;
        }
    }

    @Override
    public void setOnMouseDragEntered(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragEntered() == null) {
            super.setOnMouseDragEntered(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragEntered != null && onMouseDragEnteredEnable) {
                        beforeOnMouseDragEntered();
                        onMouseDragEntered.handle(event);
                        afterOnMouseDragEntered();
                    }

                }
            });
        }
        if (onMouseDragEntered == null) {
            onMouseDragEntered = eh;
        } else {
            onMouseDragEntered = null;
            onMouseDragEntered = eh;
        }
    }

    @Override
    public void setOnMouseDragReleased(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragReleased() == null) {
            super.setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragReleased != null && onMouseDragReleasedEnable) {
                        beforeOnMouseDragReleased();
                        onMouseDragReleased.handle(event);
                        afterOnMouseDragReleased();
                    }

                }
            });
        }
        if (onMouseDragReleased == null) {
            onMouseDragReleased = eh;
        } else {
            onMouseDragReleased = null;
            onMouseDragReleased = eh;
        }
    }

    @Override
    public void setOnMouseDragOver(EventHandler<? super MouseDragEvent> eh) {

        if (super.getOnMouseDragOver() == null) {
            super.setOnMouseDragOver(new EventHandler<MouseDragEvent>() {

                @Override
                public void handle(MouseDragEvent event) {

                    if (onMouseDragOver != null && onMouseDragOverEnable) {
                        beforeOnMouseDragOver();
                        onMouseDragOver.handle(event);
                        afterOnMouseDragOver();
                    }

                }
            });
        }
        if (onMouseDragOver == null) {
            onMouseDragOver = eh;
        } else {
            onMouseDragOver = null;
            onMouseDragOver = eh;
        }
    }

    @Override
    public void setOnDragDetected(EventHandler<? super MouseEvent> eh) {

        if (super.getOnDragDetected() == null) {
            super.setOnDragDetected(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onDragDetected != null && onDragDetectedEnable) {
                        beforeOnDragDetected();
                        onDragDetected.handle(event);
                        afterOnDragDetected();
                    }

                }
            });
        }
        if (onDragDetected == null) {
            onDragDetected = eh;
        } else {
            onDragDetected = null;
            onDragDetected = eh;
        }
    }

    @Override
    public void setOnMouseReleased(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseReleased() == null) {
            super.setOnMouseReleased(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseReleased != null && onMouseReleasedEnable) {
                        beforeOnMouseReleased();
                        onMouseReleased.handle(event);
                        afterOnMouseReleased();
                    }

                }
            });
        }
        if (onMouseReleased == null) {
            onMouseReleased = eh;
        } else {
            onMouseReleased = null;
            onMouseReleased = eh;
        }
    }

    @Override
    public void setOnMousePressed(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMousePressed() == null) {
            super.setOnMousePressed(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMousePressed != null && onMousePressedEnable) {
                        beforeOnMousePressed();
                        onMousePressed.handle(event);
                        afterOnMousePressed();
                    }

                }
            });
        }
        if (onMousePressed == null) {
            onMousePressed = eh;
        } else {
            onMousePressed = null;
            onMousePressed = eh;
        }
    }

    @Override
    public void setOnMouseMoved(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseMoved() == null) {
            super.setOnMouseMoved(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseMoved != null && onMouseMovedEnable) {
                        beforeOnMouseMoved();
                        onMouseMoved.handle(event);
                        afterOnMouseMoved();
                    }

                }
            });
        }
        if (onMouseMoved == null) {
            onMouseMoved = eh;
        } else {
            onMouseMoved = null;
            onMouseMoved = eh;
        }
    }

    @Override
    public void setOnMouseExited(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseExited() == null) {
            super.setOnMouseExited(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseExited != null && onMouseExitedEnable) {
                        beforeOnMouseExited();
                        onMouseExited.handle(event);
                        afterOnMouseExited();
                    }

                }
            });
        }
        if (onMouseExited == null) {
            onMouseExited = eh;
        } else {
            onMouseExited = null;
            onMouseExited = eh;
        }
    }

    @Override
    public void setOnMouseEntered(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseEntered() == null) {
            super.setOnMouseEntered(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseEntered != null && onMouseEnteredEnable) {
                        beforeOnMouseEntered();
                        onMouseEntered.handle(event);
                        afterOnMouseEntered();
                    }

                }
            });
        }
        if (onMouseEntered == null) {
            onMouseEntered = eh;
        } else {
            onMouseEntered = null;
            onMouseEntered = eh;
        }
    }

    @Override
    public void setOnMouseDragged(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseDragged() == null) {
            super.setOnMouseDragged(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseDragged != null && onMouseDraggedEnable) {
                        beforeOnMouseDragged();
                        onMouseDragged.handle(event);
                        afterOnMouseDragged();
                    }

                }
            });
        }
        if (onMouseDragged == null) {
            onMouseDragged = eh;
        } else {
            onMouseDragged = null;
            onMouseDragged = eh;
        }
    }

    @Override
    public void setOnMouseClicked(EventHandler<? super MouseEvent> eh) {

        if (super.getOnMouseClicked() == null) {
            super.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    if (onMouseClicked != null && onMouseClickedEnable) {
                        beforeOnMouseClicked();
                        onMouseClicked.handle(event);
                        afterOnMouseClicked();
                    }

                }
            });
        }
        if (onMouseClicked == null) {
            onMouseClicked = eh;
        } else {
            onMouseClicked = null;
            onMouseClicked = eh;
        }
    }

    @Override
    public void setOnDragDone(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragDone() == null) {
            super.setOnDragDone(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragDone != null && onDragDoneEnable) {
                        beforeOnDragDone();
                        onDragDone.handle(event);
                        afterOnDragDone();
                    }

                }
            });
        }
        if (onDragDone == null) {
            onDragDone = eh;
        } else {
            onDragDone = null;
            onDragDone = eh;
        }
    }

    @Override
    public void setOnDragDropped(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragDropped() == null) {
            super.setOnDragDropped(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragDropped != null && onDragDroppedEnable) {
                        beforeOnDragDropped();
                        onDragDropped.handle(event);
                        afterOnDragDropped();
                    }

                }
            });
        }
        if (onDragDropped == null) {
            onDragDropped = eh;
        } else {
            onDragDropped = null;
            onDragDropped = eh;
        }
    }

    @Override
    public void setOnDragOver(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragOver() == null) {
            super.setOnDragOver(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragOver != null && onDragOverEnable) {
                        beforeOnDragOver();
                        onDragOver.handle(event);
                        afterOnDragOver();
                    }

                }
            });
        }
        if (onDragOver == null) {
            onDragOver = eh;
        } else {
            onDragOver = null;
            onDragOver = eh;
        }
    }

    @Override
    public void setOnDragExited(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragExited() == null) {
            super.setOnDragExited(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragExited != null && onDragExitedEnable) {
                        beforeOnDragExited();
                        onDragExited.handle(event);
                        afterOnDragExited();
                    }

                }
            });
        }
        if (onDragExited == null) {
            onDragExited = eh;
        } else {
            onDragExited = null;
            onDragExited = eh;
        }
    }

    @Override
    public void setOnDragEntered(EventHandler<? super DragEvent> eh) {

        if (super.getOnDragEntered() == null) {
            super.setOnDragEntered(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent event) {

                    if (onDragEntered != null && onDragEnteredEnable) {
                        beforeOnDragEntered();
                        onDragEntered.handle(event);
                        afterOnDragEntered();
                    }

                }
            });
        }
        if (onDragEntered == null) {
            onDragEntered = eh;
        } else {
            onDragEntered = null;
            onDragEntered = eh;
        }
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyTyped() {
        return onKeyTyped;
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyReleased() {
        return onKeyReleased;
    }

    @Override
    public EventHandler<? super KeyEvent> getOnKeyPressed() {
        return onKeyPressed;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchStationary() {
        return onTouchStationary;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchReleased() {
        return onTouchReleased;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchMoved() {
        return onTouchMoved;
    }

    @Override
    public EventHandler<? super TouchEvent> getOnTouchPressed() {
        return onTouchPressed;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeRight() {
        return onSwipeRight;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeLeft() {
        return onSwipeLeft;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeDown() {
        return onSwipeDown;
    }

    @Override
    public EventHandler<? super SwipeEvent> getOnSwipeUp() {
        return onSwipeUp;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoomFinished() {
        return onZoomFinished;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoom() {
        return onZoom;
    }

    @Override
    public EventHandler<? super ZoomEvent> getOnZoomStarted() {
        return onZoomStarted;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotationFinished() {
        return onRotationFinished;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotate() {
        return onRotate;
    }

    @Override
    public EventHandler<? super RotateEvent> getOnRotationStarted() {
        return onRotationStarted;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScrollFinished() {
        return onScrollFinished;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScroll() {
        return onScroll;
    }

    @Override
    public EventHandler<? super ScrollEvent> getOnScrollStarted() {
        return onScrollStarted;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragExited() {
        return onMouseDragExited;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragEntered() {
        return onMouseDragEntered;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragReleased() {
        return onMouseDragReleased;
    }

    @Override
    public EventHandler<? super MouseDragEvent> getOnMouseDragOver() {
        return onMouseDragOver;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnDragDetected() {
        return onDragDetected;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseReleased() {
        return onMouseReleased;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMousePressed() {
        return onMousePressed;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseMoved() {
        return onMouseMoved;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseExited() {
        return onMouseExited;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseEntered() {
        return onMouseEntered;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseDragged() {
        return onMouseDragged;
    }

    @Override
    public EventHandler<? super MouseEvent> getOnMouseClicked() {
        return onMouseClicked;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragDone() {
        return onDragDone;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragDropped() {
        return onDragDropped;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragOver() {
        return onDragOver;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragExited() {
        return onDragExited;
    }

    @Override
    public EventHandler<? super DragEvent> getOnDragEntered() {
        return onDragEntered;
    }

    protected void beforeOnKeyTyped() {
    }

    protected void afterOnKeyTyped() {
    }

    protected void beforeOnKeyReleased() {
    }

    protected void afterOnKeyReleased() {
    }

    protected void beforeOnKeyPressed() {
    }

    protected void afterOnKeyPressed() {
    }

    protected void beforeOnTouchStationary() {
    }

    protected void afterOnTouchStationary() {
    }

    protected void beforeOnTouchReleased() {
    }

    protected void afterOnTouchReleased() {
    }

    protected void beforeOnTouchMoved() {
    }

    protected void afterOnTouchMoved() {
    }

    protected void beforeOnTouchPressed() {
    }

    protected void afterOnTouchPressed() {
    }

    protected void beforeOnSwipeRight() {
    }

    protected void afterOnSwipeRight() {
    }

    protected void beforeOnSwipeLeft() {
    }

    protected void afterOnSwipeLeft() {
    }

    protected void beforeOnSwipeDown() {
    }

    protected void afterOnSwipeDown() {
    }

    protected void beforeOnSwipeUp() {
    }

    protected void afterOnSwipeUp() {
    }

    protected void beforeOnZoomFinished() {
    }

    protected void afterOnZoomFinished() {
    }

    protected void beforeOnZoom() {
    }

    protected void afterOnZoom() {
    }

    protected void beforeOnZoomStarted() {
    }

    protected void afterOnZoomStarted() {
    }

    protected void beforeOnRotationFinished() {
    }

    protected void afterOnRotationFinished() {
    }

    protected void beforeOnRotate() {
    }

    protected void afterOnRotate() {
    }

    protected void beforeOnRotationStarted() {
    }

    protected void afterOnRotationStarted() {
    }

    protected void beforeOnScrollFinished() {
    }

    protected void afterOnScrollFinished() {
    }

    protected void beforeOnScroll() {
    }

    protected void afterOnScroll() {
    }

    protected void beforeOnScrollStarted() {
    }

    protected void afterOnScrollStarted() {
    }

    protected void beforeOnMouseDragExited() {
    }

    protected void afterOnMouseDragExited() {
    }

    protected void beforeOnMouseDragEntered() {
    }

    protected void afterOnMouseDragEntered() {
    }

    protected void beforeOnMouseDragReleased() {
    }

    protected void afterOnMouseDragReleased() {
    }

    protected void beforeOnMouseDragOver() {
    }

    protected void afterOnMouseDragOver() {
    }

    protected void beforeOnDragDetected() {
    }

    protected void afterOnDragDetected() {
    }

    protected void beforeOnMouseReleased() {
    }

    protected void afterOnMouseReleased() {
    }

    protected void beforeOnMousePressed() {
    }

    protected void afterOnMousePressed() {
    }

    protected void beforeOnMouseMoved() {
    }

    protected void afterOnMouseMoved() {
    }

    protected void beforeOnMouseExited() {
    }

    protected void afterOnMouseExited() {
    }

    protected void beforeOnMouseEntered() {
    }

    protected void afterOnMouseEntered() {
    }

    protected void beforeOnMouseDragged() {
    }

    protected void afterOnMouseDragged() {
    }

    protected void beforeOnMouseClicked() {
    }

    protected void afterOnMouseClicked() {
    }

    protected void beforeOnDragDone() {
    }

    protected void afterOnDragDone() {
    }

    protected void beforeOnDragDropped() {
    }

    protected void afterOnDragDropped() {
    }

    protected void beforeOnDragOver() {
    }

    protected void afterOnDragOver() {
    }

    protected void beforeOnDragExited() {
    }

    protected void afterOnDragExited() {
    }

    protected void beforeOnDragEntered() {
    }

    protected void afterOnDragEntered() {
    }

    public void sowp(Node n, int j) {

        int i = getChildren().indexOf(n);
        j = i + j;

        if (i < getChildren().size() && j < getChildren().size() && i > -1 && j > -1) {
            Node n1 = getChildren().get(i);
            Node n2 = getChildren().get(j);

            Group q = new Group();
            getChildren().set(i, q);
            getChildren().set(j, n1);
            getChildren().set(i, n2);
        }
    }

    

   
}
