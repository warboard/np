package view.container;

import com.sun.javafx.sg.prism.NGNode;

import java.util.List;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.geometry.VPos;
import javafx.scene.CacheHint;
import javafx.scene.Cursor;
import javafx.scene.DepthTest;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.SnapshotResult;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.Effect;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.InputMethodRequests;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.input.ZoomEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Transform;
import javafx.util.Callback;

public class MyAnchorPane {

    protected AnchorPane anchorPane;

    public MyAnchorPane() {
        anchorPane = new AnchorPane();

    }

    public MyAnchorPane(Node[] nodes) {
        anchorPane = new AnchorPane(nodes);
    }

    protected ObservableList<Node> getChildren() {
        return anchorPane.getChildren();
    }

    public double prefWidth(double d) {
        return anchorPane.prefWidth(d);
    }

    public double prefHeight(double d) {
        return anchorPane.prefHeight(d);
    }

    protected ObservableList<Node> getChildrenUnmodifiable() {
        return anchorPane.getChildrenUnmodifiable();
    }

    public Node lookup(String string) {
        return anchorPane.lookup(string);
    }

    public boolean isNeedsLayout() {
        return anchorPane.isNeedsLayout();
    }

    public ReadOnlyBooleanProperty needsLayoutProperty() {
        return anchorPane.needsLayoutProperty();
    }

    public void requestLayout() {
        anchorPane.requestLayout();
    }

    public double minWidth(double d) {
        return anchorPane.minWidth(d);
    }

    public double minHeight(double d) {
        return anchorPane.minHeight(d);
    }

    public double getBaselineOffset() {
        return anchorPane.getBaselineOffset();
    }

    public void layout() {
        anchorPane.layout();
    }

    public ObservableList<String> getStylesheets() {
        return anchorPane.getStylesheets();
    }

    public AnchorPane getPNode() {
        return anchorPane;
    }

    public ObservableMap<Object, Object> getProperties() {
        return anchorPane.getProperties();
    }

    public boolean hasProperties() {
        return anchorPane.hasProperties();
    }

    public void setUserData(Object o) {

    }

    public Object getUserData() {
        return anchorPane.getUserData();
    }

    void setParent(Parent parent) {

    }

    public Parent getParent() {
        return anchorPane.getParent();
    }

    public ReadOnlyObjectProperty<Parent> parentProperty() {
        return anchorPane.parentProperty();
    }

    public Scene getScene() {
        return anchorPane.getScene();
    }

    public ReadOnlyObjectProperty<Scene> sceneProperty() {
        return anchorPane.sceneProperty();
    }

    public void setId(String string) {
        anchorPane.setId(string);
    }

    public String getId() {
        return anchorPane.getId();
    }

    public StringProperty idProperty() {
        return anchorPane.idProperty();
    }

    public ObservableList<String> getStyleClass() {
        return anchorPane.getStyleClass();
    }

    public void setStyle(String string) {
        anchorPane.setStyle(string);
    }

    public String getStyle() {
        return anchorPane.getStyle();
    }

    public StringProperty styleProperty() {
        return anchorPane.styleProperty();
    }

    public void setVisible(boolean bln) {
        anchorPane.setVisible(bln);
    }

    public boolean isVisible() {
        return anchorPane.isVisible();
    }

    public BooleanProperty visibleProperty() {
        return anchorPane.visibleProperty();
    }

    public void setCursor(Cursor cursor) {
        anchorPane.setCursor(cursor);
    }

    public Cursor getCursor() {
        return anchorPane.getCursor();
    }

    public ObjectProperty<Cursor> cursorProperty() {
        return anchorPane.cursorProperty();
    }

    public void setOpacity(double d) {
        anchorPane.setOpacity(d);
    }

    public double getOpacity() {
        return anchorPane.getOpacity();
    }

    public DoubleProperty opacityProperty() {
        return anchorPane.opacityProperty();
    }

    public void setBlendMode(BlendMode bm) {
        anchorPane.setBlendMode(bm);
    }

    public BlendMode getBlendMode() {
        return anchorPane.getBlendMode();

    }

    public ObjectProperty<BlendMode> blendModeProperty() {
        return anchorPane.blendModeProperty();
    }

    public void setClip(Node node) {
        anchorPane.setClip(node);
    }

    public Node getClip() {
        return anchorPane.getClip();
    }

    public ObjectProperty<Node> clipProperty() {
        return anchorPane.clipProperty();
    }

    public void setCache(boolean bln) {
        anchorPane.setCache(bln);
    }

    public boolean isCache() {
        return anchorPane.isCache();
    }

    public BooleanProperty cacheProperty() {
        return anchorPane.cacheProperty();
    }

    public void setCacheHint(CacheHint ch) {
        anchorPane.setCacheHint(ch);
    }

    public CacheHint getCacheHint() {
        return anchorPane.getCacheHint();
    }

    public ObjectProperty<CacheHint> cacheHintProperty() {
        return anchorPane.cacheHintProperty();
    }

    public void setEffect(Effect effect) {
        anchorPane.setEffect(effect);
    }

    public Effect getEffect() {
        return anchorPane.getEffect();

    }

    public ObjectProperty<Effect> effectProperty() {
        return anchorPane.effectProperty();
    }

    public void setDepthTest(DepthTest dt) {
        anchorPane.setDepthTest(dt);
    }

    public DepthTest getDepthTest() {
        return anchorPane.getDepthTest();
    }

    public ObjectProperty<DepthTest> depthTestProperty() {
        return anchorPane.depthTestProperty();
    }

    public void setDisable(boolean bln) {
        anchorPane.setDisable(bln);
    }

    public boolean isDisable() {
        return anchorPane.isDisable();

    }

    public BooleanProperty disableProperty() {
        return anchorPane.disableProperty();
    }

    public void setPickOnBounds(boolean bln) {
        anchorPane.setPickOnBounds(bln);
    }

    public boolean isPickOnBounds() {
        return anchorPane.isPickOnBounds();
    }

    public BooleanProperty pickOnBoundsProperty() {
        return anchorPane.pickOnBoundsProperty();
    }

    public boolean isDisabled() {
        return anchorPane.isDisabled();
    }

    public ReadOnlyBooleanProperty disabledProperty() {
        return anchorPane.disabledProperty();
    }

    public Set<Node> lookupAll(String string) {
        return anchorPane.lookupAll(string);
    }

    public WritableImage snapshot(SnapshotParameters sp, WritableImage wi) {
        return anchorPane.snapshot(sp, wi);
    }

    public void snapshot(Callback<SnapshotResult, Void> clbck, SnapshotParameters sp, WritableImage wi) {
        anchorPane.snapshot(clbck, sp, wi);
    }

    public void setOnDragEntered(EventHandler<? super DragEvent> eh) {
        anchorPane.setOnDragEntered(eh);
    }

    public EventHandler<? super DragEvent> getOnDragEntered() {
        return anchorPane.getOnDragEntered();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragEnteredProperty() {
        return anchorPane.onDragEnteredProperty();
    }

    public void setOnDragExited(EventHandler<? super DragEvent> eh) {
        anchorPane.setOnDragExited(eh);
    }

    public EventHandler<? super DragEvent> getOnDragExited() {
        return anchorPane.getOnDragExited();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragExitedProperty() {
        return anchorPane.onDragExitedProperty();
    }

    public void setOnDragOver(EventHandler<? super DragEvent> eh) {
        anchorPane.setOnDragOver(eh);
    }

    public EventHandler<? super DragEvent> getOnDragOver() {
        return anchorPane.getOnDragOver();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragOverProperty() {
        return anchorPane.onDragOverProperty();
    }

    public void setOnDragDropped(EventHandler<? super DragEvent> eh) {
        anchorPane.setOnDragDropped(eh);
    }

    public EventHandler<? super DragEvent> getOnDragDropped() {
        return anchorPane.getOnDragDropped();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragDroppedProperty() {
        return anchorPane.onDragDroppedProperty();
    }

    public void setOnDragDone(EventHandler<? super DragEvent> eh) {
        anchorPane.setOnDragDone(eh);
    }

    public EventHandler<? super DragEvent> getOnDragDone() {
        return anchorPane.getOnDragDone();
    }

    public ObjectProperty<EventHandler<? super DragEvent>> onDragDoneProperty() {
        return anchorPane.onDragDoneProperty();
    }

    public Dragboard startDragAndDrop(TransferMode[] tms) {
        return anchorPane.startDragAndDrop(tms);
    }

    public void startFullDrag() {
        anchorPane.startFullDrag();
    }

    public void setManaged(boolean bln) {
        anchorPane.setManaged(bln);
    }

    public boolean isManaged() {
        return anchorPane.isManaged();
    }

    public BooleanProperty managedProperty() {
        return anchorPane.managedProperty();
    }

    public void setLayoutX(double d) {
        anchorPane.setLayoutX(d);
    }

    public double getLayoutX() {
        return anchorPane.getLayoutX();
    }

    public DoubleProperty layoutXProperty() {
        return anchorPane.layoutXProperty();
    }

    public void setLayoutY(double d) {
        anchorPane.setLayoutY(d);
    }

    public double getLayoutY() {
        return anchorPane.getLayoutY();
    }

    public DoubleProperty layoutYProperty() {
        return anchorPane.layoutYProperty();
    }

    public void relocate(double d, double d1) {
        anchorPane.relocate(d, d1);
    }

    public boolean isResizable() {
        return anchorPane.isResizable();
    }

    public Orientation getContentBias() {
        return anchorPane.getContentBias();
    }

    public double maxWidth(double d) {
        return anchorPane.maxWidth(d);
    }

    public double maxHeight(double d) {
        return anchorPane.maxHeight(d);
    }

    public void resize(double d, double d1) {
        anchorPane.resize(d, d1);
    }

    public void autosize() {
        anchorPane.autosize();
    }

    public void resizeRelocate(double x, double y, double w, double h) {
        anchorPane.resizeRelocate(x, y, w, h);
    }

    public Bounds getBoundsInParent() {
        return anchorPane.getBoundsInParent();
    }

    public ReadOnlyObjectProperty<Bounds> boundsInParentProperty() {
        return anchorPane.boundsInParentProperty();
    }

    public Bounds getBoundsInLocal() {
        return anchorPane.getBoundsInLocal();
    }

    public ReadOnlyObjectProperty<Bounds> boundsInLocalProperty() {
        return anchorPane.boundsInLocalProperty();
    }

    public Bounds getLayoutBounds() {
        return anchorPane.getLayoutBounds();
    }

    public ReadOnlyObjectProperty<Bounds> layoutBoundsProperty() {
        return anchorPane.layoutBoundsProperty();
    }

    public boolean contains(double x, double y) {
        return anchorPane.contains(x, y);
    }

    public boolean contains(Point2D xy) {
        return anchorPane.contains(xy);
    }

    public boolean intersects(double x, double y, double w, double h) {
        return anchorPane.intersects(x, y, w, h);
    }

    public boolean intersects(Bounds bounds) {
        return anchorPane.intersects(bounds);
    }

    public Point2D sceneToLocal(double d, double d1) {
        return anchorPane.sceneToLocal(d, d1);
    }

    public Point2D sceneToLocal(Point2D pd) {
        return anchorPane.sceneToLocal(pd);
    }

    public Bounds sceneToLocal(Bounds bounds) {
        return anchorPane.sceneToLocal(bounds);
    }

    public Point2D localToScene(double d, double d1) {
        return anchorPane.localToParent(d, d1);
    }

    public Point2D localToScene(Point2D pd) {
        return anchorPane.localToParent(pd);
    }

    public Bounds localToScene(Bounds bounds) {
        return anchorPane.localToParent(bounds);
    }

    public Point2D parentToLocal(double d, double d1) {
        return anchorPane.parentToLocal(d, d1);
    }

    public Point2D parentToLocal(Point2D pd) {
        return anchorPane.parentToLocal(pd);
    }

    public Bounds parentToLocal(Bounds bounds) {
        return anchorPane.parentToLocal(bounds);
    }

    public Point2D localToParent(double d, double d1) {
        return anchorPane.localToParent(d, d1);
    }

    public Point2D localToParent(Point2D pd) {
        return anchorPane.localToParent(pd);
    }

    public Bounds localToParent(Bounds bounds) {
        return anchorPane.localToParent(bounds);
    }

    public ObservableList<Transform> getTransforms() {
        return anchorPane.getTransforms();
    }

    public void setTranslateX(double d) {
        anchorPane.setTranslateX(d);
    }

    public double getTranslateX() {
        return anchorPane.getTranslateX();
    }

    public DoubleProperty translateXProperty() {
        return anchorPane.translateXProperty();
    }

    public void setTranslateY(double d) {
        anchorPane.setTranslateY(d);
    }

    public double getTranslateY() {
        return anchorPane.getTranslateY();
    }

    public DoubleProperty translateYProperty() {
        return anchorPane.translateYProperty();
    }

    public void setTranslateZ(double d) {
        anchorPane.setTranslateZ(d);
    }

    public double getTranslateZ() {
        return anchorPane.getTranslateZ();
    }

    public DoubleProperty translateZProperty() {
        return anchorPane.translateZProperty();
    }

    public void setScaleX(double d) {
        anchorPane.setScaleX(d);
    }

    public double getScaleX() {
        return anchorPane.getScaleX();
    }

    public DoubleProperty scaleXProperty() {
        return anchorPane.scaleXProperty();
    }

    public void setScaleY(double d) {
        anchorPane.setScaleY(d);

    }

    public double getScaleY() {
        return anchorPane.getScaleY();
    }

    public DoubleProperty scaleYProperty() {
        return anchorPane.scaleYProperty();
    }

    public void setScaleZ(double d) {
        anchorPane.setScaleZ(d);
    }

    public double getScaleZ() {
        return anchorPane.getScaleZ();
    }

    public DoubleProperty scaleZProperty() {
        return scaleZProperty();
    }

    public void setRotate(double d) {
        anchorPane.setRotate(d);
    }

    public double getRotate() {
        return anchorPane.getRotate();
    }

    public DoubleProperty rotateProperty() {
        return anchorPane.rotateProperty();

    }

    public void setRotationAxis(Point3D pd) {
        anchorPane.setRotationAxis(pd);
    }

    public Point3D getRotationAxis() {
        return anchorPane.getRotationAxis();
    }

    public ObjectProperty<Point3D> rotationAxisProperty() {
        return anchorPane.rotationAxisProperty();
    }

    public ReadOnlyObjectProperty<Transform> localToParentTransformProperty() {
        return anchorPane.localToParentTransformProperty();
    }

    public Transform getLocalToParentTransform() {
        return anchorPane.getLocalToParentTransform();
    }

    public ReadOnlyObjectProperty<Transform> localToSceneTransformProperty() {
        return anchorPane.localToSceneTransformProperty();
    }

    public Transform getLocalToSceneTransform() {
        return anchorPane.getLocalToSceneTransform();
    }

    public void setMouseTransparent(boolean bln) {
        anchorPane.setMouseTransparent(bln);
    }

    public boolean isMouseTransparent() {
        return anchorPane.isMouseTransparent();
    }

    public BooleanProperty mouseTransparentProperty() {
        return anchorPane.mouseTransparentProperty();
    }

    public boolean isHover() {
        return anchorPane.isHover();
    }

    public ReadOnlyBooleanProperty hoverProperty() {
        return anchorPane.hoverProperty();
    }

    public boolean isPressed() {
        return anchorPane.isPressed();
    }

    public ReadOnlyBooleanProperty pressedProperty() {
        return anchorPane.pressedProperty();
    }

    public void setOnContextMenuRequested(EventHandler<? super ContextMenuEvent> eh) {
        anchorPane.setOnContextMenuRequested(eh);
    }

    public EventHandler<? super ContextMenuEvent> getOnContextMenuRequested() {
        return anchorPane.getOnContextMenuRequested();
    }

    public ObjectProperty<EventHandler<? super ContextMenuEvent>> onContextMenuRequestedProperty() {
        return anchorPane.onContextMenuRequestedProperty();
    }

    public void setOnMouseClicked(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseClicked(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseClicked() {
        return anchorPane.getOnMouseClicked();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseClickedProperty() {
        return anchorPane.onMouseClickedProperty();
    }

    public void setOnMouseDragged(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseDragged(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseDragged() {
        return anchorPane.getOnMouseDragged();

    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseDraggedProperty() {
        return anchorPane.onMouseDraggedProperty();
    }

    public void setOnMouseEntered(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseEntered(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseEntered() {
        return anchorPane.getOnMouseEntered();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseEnteredProperty() {
        return anchorPane.onMouseEnteredProperty();
    }

    public void setOnMouseExited(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseExited(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseExited() {
        return anchorPane.getOnMouseExited();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseExitedProperty() {
        return anchorPane.onMouseExitedProperty();
    }

    public void setOnMouseMoved(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseMoved(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseMoved() {
        return anchorPane.getOnMouseMoved();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseMovedProperty() {
        return anchorPane.onMouseMovedProperty();
    }

    public void setOnMousePressed(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMousePressed(eh);
    }

    public EventHandler<? super MouseEvent> getOnMousePressed() {
        return anchorPane.getOnMousePressed();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMousePressedProperty() {
        return anchorPane.onMousePressedProperty();
    }

    public void setOnMouseReleased(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnMouseReleased(eh);
    }

    public EventHandler<? super MouseEvent> getOnMouseReleased() {
        return anchorPane.getOnMouseReleased();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onMouseReleasedProperty() {
        return anchorPane.onMouseReleasedProperty();
    }

    public void setOnDragDetected(EventHandler<? super MouseEvent> eh) {
        anchorPane.setOnDragDetected(eh);
    }

    public EventHandler<? super MouseEvent> getOnDragDetected() {
        return anchorPane.getOnDragDetected();
    }

    public ObjectProperty<EventHandler<? super MouseEvent>> onDragDetectedProperty() {
        return anchorPane.onDragDetectedProperty();
    }

    public void setOnMouseDragOver(EventHandler<? super MouseDragEvent> eh) {
        anchorPane.setOnMouseDragOver(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragOver() {
        return anchorPane.getOnMouseDragOver();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragOverProperty() {
        return anchorPane.onMouseDragOverProperty();
    }

    public void setOnMouseDragReleased(EventHandler<? super MouseDragEvent> eh) {
        anchorPane.setOnMouseDragReleased(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragReleased() {
        return anchorPane.getOnMouseDragReleased();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragReleasedProperty() {
        return anchorPane.onMouseDragReleasedProperty();
    }

    public void setOnMouseDragEntered(EventHandler<? super MouseDragEvent> eh) {
        anchorPane.setOnMouseDragEntered(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragEntered() {
        return anchorPane.getOnMouseDragEntered();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragEnteredProperty() {
        return anchorPane.onMouseDragEnteredProperty();
    }

    public void setOnMouseDragExited(EventHandler<? super MouseDragEvent> eh) {
        anchorPane.setOnMouseDragExited(eh);
    }

    public EventHandler<? super MouseDragEvent> getOnMouseDragExited() {
        return anchorPane.getOnMouseDragExited();
    }

    public ObjectProperty<EventHandler<? super MouseDragEvent>> onMouseDragExitedProperty() {
        return anchorPane.onMouseDragExitedProperty();
    }

    public void setOnScrollStarted(EventHandler<? super ScrollEvent> eh) {
        anchorPane.setOnScroll(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScrollStarted() {
        return anchorPane.getOnScrollStarted();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollStartedProperty() {
        return anchorPane.onScrollStartedProperty();
    }

    public void setOnScroll(EventHandler<? super ScrollEvent> eh) {
        anchorPane.setOnScroll(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScroll() {
        return anchorPane.getOnScroll();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollProperty() {
        return anchorPane.onScrollProperty();
    }

    public void setOnScrollFinished(EventHandler<? super ScrollEvent> eh) {
        anchorPane.setOnScrollFinished(eh);
    }

    public EventHandler<? super ScrollEvent> getOnScrollFinished() {
        return anchorPane.getOnScrollFinished();
    }

    public ObjectProperty<EventHandler<? super ScrollEvent>> onScrollFinishedProperty() {
        return anchorPane.onScrollFinishedProperty();
    }

    public void setOnRotationStarted(EventHandler<? super RotateEvent> eh) {
        anchorPane.setOnRotationStarted(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotationStarted() {
        return anchorPane.getOnRotationStarted();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotationStartedProperty() {
        return anchorPane.onRotationStartedProperty();
    }

    public void setOnRotate(EventHandler<? super RotateEvent> eh) {
        anchorPane.setOnRotate(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotate() {
        return anchorPane.getOnRotate();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotateProperty() {
        return anchorPane.onRotateProperty();
    }

    public void setOnRotationFinished(EventHandler<? super RotateEvent> eh) {
        anchorPane.setOnRotationFinished(eh);
    }

    public EventHandler<? super RotateEvent> getOnRotationFinished() {
        return anchorPane.getOnRotationFinished();
    }

    public ObjectProperty<EventHandler<? super RotateEvent>> onRotationFinishedProperty() {
        return anchorPane.onRotationFinishedProperty();
    }

    public void setOnZoomStarted(EventHandler<? super ZoomEvent> eh) {
        anchorPane.setOnZoomStarted(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoomStarted() {
        return anchorPane.getOnZoomStarted();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomStartedProperty() {
        return anchorPane.onZoomStartedProperty();
    }

    public void setOnZoom(EventHandler<? super ZoomEvent> eh) {
        anchorPane.setOnZoom(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoom() {
        return anchorPane.getOnZoom();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomProperty() {
        return anchorPane.onZoomProperty();
    }

    public void setOnZoomFinished(EventHandler<? super ZoomEvent> eh) {
        anchorPane.setOnZoomFinished(eh);
    }

    public EventHandler<? super ZoomEvent> getOnZoomFinished() {
        return anchorPane.getOnZoomFinished();
    }

    public ObjectProperty<EventHandler<? super ZoomEvent>> onZoomFinishedProperty() {
        return anchorPane.onZoomFinishedProperty();
    }

    public void setOnSwipeUp(EventHandler<? super SwipeEvent> eh) {
        anchorPane.setOnSwipeUp(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeUp() {
        return anchorPane.getOnSwipeUp();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeUpProperty() {
        return anchorPane.onSwipeUpProperty();
    }

    public void setOnSwipeDown(EventHandler<? super SwipeEvent> eh) {
        anchorPane.setOnSwipeDown(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeDown() {
        return anchorPane.getOnSwipeDown();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeDownProperty() {
        return anchorPane.onSwipeDownProperty();
    }

    public void setOnSwipeLeft(EventHandler<? super SwipeEvent> eh) {
        anchorPane.setOnSwipeLeft(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeLeft() {
        return anchorPane.getOnSwipeLeft();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeLeftProperty() {
        return anchorPane.onSwipeLeftProperty();
    }

    public void setOnSwipeRight(EventHandler<? super SwipeEvent> eh) {
        anchorPane.setOnSwipeRight(eh);
    }

    public EventHandler<? super SwipeEvent> getOnSwipeRight() {
        return anchorPane.getOnSwipeRight();
    }

    public ObjectProperty<EventHandler<? super SwipeEvent>> onSwipeRightProperty() {
        return anchorPane.onSwipeRightProperty();
    }

    public void setOnTouchPressed(EventHandler<? super TouchEvent> eh) {
        anchorPane.setOnTouchPressed(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchPressed() {
        return anchorPane.getOnTouchPressed();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchPressedProperty() {
        return anchorPane.onTouchPressedProperty();
    }

    public void setOnTouchMoved(EventHandler<? super TouchEvent> eh) {
        anchorPane.setOnTouchMoved(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchMoved() {
        return anchorPane.getOnTouchMoved();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchMovedProperty() {
        return anchorPane.onTouchMovedProperty();
    }

    public void setOnTouchReleased(EventHandler<? super TouchEvent> eh) {
        anchorPane.setOnTouchReleased(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchReleased() {
        return anchorPane.getOnTouchReleased();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchReleasedProperty() {
        return anchorPane.onTouchReleasedProperty();
    }

    public void setOnTouchStationary(EventHandler<? super TouchEvent> eh) {
        anchorPane.setOnTouchStationary(eh);
    }

    public EventHandler<? super TouchEvent> getOnTouchStationary() {
        return anchorPane.getOnTouchStationary();
    }

    public ObjectProperty<EventHandler<? super TouchEvent>> onTouchStationaryProperty() {
        return anchorPane.onTouchStationaryProperty();
    }

    public void setOnKeyPressed(EventHandler<? super KeyEvent> eh) {
        anchorPane.setOnKeyPressed(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyPressed() {
        return anchorPane.getOnKeyPressed();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyPressedProperty() {
        return anchorPane.onKeyPressedProperty();
    }

    public void setOnKeyReleased(EventHandler<? super KeyEvent> eh) {
        anchorPane.setOnKeyReleased(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyReleased() {
        return anchorPane.getOnKeyReleased();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyReleasedProperty() {
        return anchorPane.onKeyReleasedProperty();
    }

    public void setOnKeyTyped(EventHandler<? super KeyEvent> eh) {
        anchorPane.setOnKeyTyped(eh);
    }

    public EventHandler<? super KeyEvent> getOnKeyTyped() {
        return anchorPane.getOnKeyTyped();
    }

    public ObjectProperty<EventHandler<? super KeyEvent>> onKeyTypedProperty() {
        return anchorPane.onKeyTypedProperty();
    }

    public void setOnInputMethodTextChanged(EventHandler<? super InputMethodEvent> eh) {
        anchorPane.setOnInputMethodTextChanged(eh);
    }

    public EventHandler<? super InputMethodEvent> getOnInputMethodTextChanged() {
        return anchorPane.getOnInputMethodTextChanged();
    }

    public ObjectProperty<EventHandler<? super InputMethodEvent>> onInputMethodTextChangedProperty() {
        return anchorPane.onInputMethodTextChangedProperty();
    }

    public void setInputMethodRequests(InputMethodRequests imr) {
        anchorPane.setInputMethodRequests(imr);
    }

    public InputMethodRequests getInputMethodRequests() {
        return anchorPane.getInputMethodRequests();
    }

    public ObjectProperty<InputMethodRequests> inputMethodRequestsProperty() {
        return anchorPane.inputMethodRequestsProperty();
    }

    public boolean isFocused() {
        return anchorPane.isFocused();
    }

    public ReadOnlyBooleanProperty focusedProperty() {
        return anchorPane.focusedProperty();
    }

    public void setFocusTraversable(boolean bln) {
        anchorPane.setFocusTraversable(bln);
    }

    public boolean isFocusTraversable() {
        return anchorPane.isFocusTraversable();
    }

    public BooleanProperty focusTraversableProperty() {
        return anchorPane.focusTraversableProperty();
    }

    public void requestFocus() {
        anchorPane.requestFocus();
    }

    public String toString() {
        return anchorPane.toString();
    }

    public void setEventDispatcher(EventDispatcher ed) {
        anchorPane.setEventDispatcher(ed);
    }

    public EventDispatcher getEventDispatcher() {
        return anchorPane.getEventDispatcher();
    }

    public ObjectProperty<EventDispatcher> eventDispatcherProperty() {
        return anchorPane.eventDispatcherProperty();
    }

    public <T extends Event> void addEventHandler(EventType<T> et, EventHandler<? super T> eh) {
        anchorPane.addEventHandler(et, eh);
    }

    public <T extends Event> void removeEventHandler(EventType<T> et, EventHandler<? super T> eh) {
        anchorPane.removeEventHandler(et, eh);
    }

    public <T extends Event> void addEventFilter(EventType<T> et, EventHandler<? super T> eh) {
        anchorPane.addEventFilter(et, eh);
    }

    public <T extends Event> void removeEventFilter(EventType<T> et, EventHandler<? super T> eh) {
        anchorPane.removeEventFilter(et, eh);
    }

    public EventDispatchChain buildEventDispatchChain(EventDispatchChain edc) {
        return anchorPane.buildEventDispatchChain(edc);
    }

    public void fireEvent(Event event) {
        anchorPane.fireEvent(event);
    }

    
    
    
    
    
    
    public static void setTopAnchor(Node child, Double value) {
        AnchorPane.setTopAnchor(child, value);
    }

    public static Double getTopAnchor(Node child) {
        return AnchorPane.getTopAnchor(child);
    }

    public static void setLeftAnchor(Node child, Double value) {
        AnchorPane.setLeftAnchor(child, value);
    }

    public static Double getLeftAnchor(Node child) {
        return AnchorPane.getLeftAnchor(child);
    }

    public static void setBottomAnchor(Node child, Double value) {
        AnchorPane.setBottomAnchor(child, value);
    }

    public static Double getBottomAnchor(Node child) {
        return AnchorPane.getBottomAnchor(child);
    }

   
    public static void setRightAnchor(Node child, Double value) {
        AnchorPane.setRightAnchor(child, value);
    }


    public static Double getRightAnchor(Node child) {
        return AnchorPane.getRightAnchor(child);
    }

    public static void clearConstraints(Node child) {
        setTopAnchor(child, null);
        setRightAnchor(child, null);
        setBottomAnchor(child, null);
        setLeftAnchor(child, null);
    }
    
    
    
   
   

    public  boolean isSnapToPixel() { 
        return anchorPane.isSnapToPixel(); 
    }
    public  void setSnapToPixel(boolean value) {
        anchorPane.setSnapToPixel(value);
    }
    public  BooleanProperty snapToPixelProperty() {
       return anchorPane.snapToPixelProperty();
    }

    public  void setPadding(Insets value) {
        anchorPane.setPadding(value);
    }
    public  Insets getPadding() {
        return anchorPane.getPadding(); 
    }
    public  ObjectProperty<Insets> paddingProperty() {
        return anchorPane.paddingProperty(); 
    }

    
    public  void setBackground(Background value) {
        anchorPane.setBackground(value);
    }
    public  Background getBackground() {
        return anchorPane.getBackground(); 
    }
    public  ObjectProperty<Background> backgroundProperty() {
        return anchorPane.backgroundProperty(); 
    }

    public  void setBorder(Border value) { anchorPane.setBorder(value); }
    public  Border getBorder() { return anchorPane.getBorder(); }
    public  ObjectProperty<Border> borderProperty() { return anchorPane.borderProperty(); }

   
    public  ObjectProperty<Insets> opaqueInsetsProperty() {
        
        return anchorPane.opaqueInsetsProperty();
    }
    public  void setOpaqueInsets(Insets value) { 
        anchorPane.setOpaqueInsets(value);
    }
    public  Insets getOpaqueInsets() { 
        return anchorPane.getOpaqueInsets();
    }

    public  Insets getInsets() { 
        return anchorPane.getInsets();
    }
    public  ReadOnlyObjectProperty<Insets> insetsProperty() {
        return anchorPane.insetsProperty();
    }


   

    public double getWidth() { return anchorPane.getWidth(); }

    public ReadOnlyDoubleProperty widthProperty() {
       return anchorPane.widthProperty();
    }


    public  double getHeight() { 
        return anchorPane.getHeight();
    }

    public  ReadOnlyDoubleProperty heightProperty() {
        return heightProperty();
    }

    public void setMinWidth(double value) {
       anchorPane.setMinWidth(value);
    }
    public  double getMinWidth() { 
    return anchorPane.getMinWidth();
    }
    public  DoubleProperty minWidthProperty() {
        return anchorPane.minWidthProperty();
    }

    public  void setMinHeight(double value) {
        anchorPane.setMinHeight(value);
    }
    public  double getMinHeight() { 
        
        return anchorPane.getMinHeight();
    }
    public  DoubleProperty minHeightProperty() {
        return anchorPane.minHeightProperty();
    }

    public void setMinSize(double minWidth, double minHeight) {
        anchorPane.setMinSize(minWidth, minHeight);
    }


    public  void setPrefWidth(double value) {
       anchorPane.setPrefWidth(value);
    }
    public  double getPrefWidth() {
        return anchorPane.getPrefWidth();
    }
    public  DoubleProperty prefWidthProperty() {
        return anchorPane.prefWidthProperty();
    }

    public  void setPrefHeight(double value) {
       anchorPane.setPrefHeight(value);
    }
    public  double getPrefHeight() {
    return anchorPane.getPrefHeight();
    }
    public  DoubleProperty prefHeightProperty() {
        return anchorPane.prefHeightProperty();
    }

    public void setPrefSize(double prefWidth, double prefHeight) {
        anchorPane.setPrefSize(prefWidth, prefHeight);
    }

    public  void setMaxWidth(double value) {
       anchorPane.setMaxWidth(value);
    }
    public  double getMaxWidth() {
       return anchorPane.getMaxWidth();
    }
    public  DoubleProperty maxWidthProperty() {
        return anchorPane.maxWidthProperty();
    }

   
    public  void setMaxHeight(double value) {
        anchorPane.setMaxHeight(value);
    }
    public  double getMaxHeight() {
        return anchorPane.getMaxHeight();
    }
    public  DoubleProperty maxHeightProperty() {
        return anchorPane.maxHeightProperty();
    }

    public void setMaxSize(double maxWidth, double maxHeight) {
        anchorPane.setMaxSize(maxWidth, maxHeight);
    }

    public  Shape getShape() { 
        return anchorPane.getShape();
    }
    public  void setShape(Shape value) { 
        anchorPane.setShape(value);
    }
    
    
    public  ObjectProperty<Shape> shapeProperty() {
        return anchorPane.shapeProperty();
    }

    public  void setScaleShape(boolean value) {
        anchorPane.setScaleShape(value);
    }
    public  boolean isScaleShape() { 
        return anchorPane.isScaleShape();
            }
    public  BooleanProperty scaleShapeProperty() {
        return anchorPane.scaleShapeProperty();
    }

   
    public  void setCenterShape(boolean value) {
        anchorPane.setCenterShape(value);
    }
    public  boolean isCenterShape() { 
        return anchorPane.isCacheShape();
        
    }
    public  BooleanProperty centerShapeProperty() {
        return anchorPane.centerShapeProperty();
    }

   
    public  void setCacheShape(boolean value) { 
        anchorPane.setCache(value);
    }
    public  boolean isCacheShape() {
        return anchorPane.isCacheShape();
    }
    public  BooleanProperty cacheShapeProperty() {
       return anchorPane.cacheShapeProperty();
    }



    


  
    public  double snappedTopInset() {
        return anchorPane.snappedTopInset();
    }

    public  double snappedBottomInset() {
        return anchorPane.snappedBottomInset();
    }

    public  double snappedLeftInset() {
        return anchorPane.snappedLeftInset();
    }

    
    public  double snappedRightInset() {
        return anchorPane.snappedRightInset();
    }


   

   
    public static void positionInArea(Node child, double areaX, double areaY, double areaWidth, double areaHeight,
                               double areaBaselineOffset, Insets margin, HPos halignment, VPos valignment, boolean isSnapToPixel) {
        AnchorPane.positionInArea(child, areaX, areaY, areaWidth, areaHeight, areaBaselineOffset, margin, halignment, valignment, isSnapToPixel);
    }

    public static void layoutInArea(Node child, double areaX, double areaY,
                               double areaWidth, double areaHeight,
                               double areaBaselineOffset,
                               Insets margin, boolean fillWidth, boolean fillHeight,
                               HPos halignment, VPos valignment, boolean isSnapToPixel) {
        AnchorPane.layoutInArea(child, areaX, areaY, areaWidth, areaHeight, areaBaselineOffset, margin, fillWidth, fillHeight, halignment, valignment, isSnapToPixel);
        }


        

    public void impl_updatePeer() {
        anchorPane.impl_updatePeer();
    }

    public NGNode impl_createPeer() {
        return anchorPane.impl_createPeer();
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return AnchorPane.getClassCssMetaData();
    }

    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        return anchorPane.getCssMetaData();
    }

}
