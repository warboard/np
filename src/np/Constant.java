/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import javafx.scene.paint.Color;

/**
 *
 * @author Azhir-user
 */
public class Constant {

    public static String FREE = "Free";
    public static String WHITE = "White";
    public static String BEAR = "Bear";
    public static String FISH = "Fish";

    public static String ICE = "Ice";
    public static String WATER = "Water";

    public static Color TRUE_COLOR = Color.AQUA;
    public static Color FALSE_COLOR = Color.RED;

    public static String[] mapData = {"",
        "1-1-0-1-1-0-1-0-0-1-0-0-0-0-0-1",//1
        "1-1-1-0-1-1-1-1-0-1-0-1-0-0-1-0",//2
        "0-1-0-0-1-0-1-1-1-1-0-1-1-1-1-0",//3
        "0-1-0-1-1-0-0-1-0-1-1-0-1-1-1-1",//4
        "0-0-1-1-0-0-0-1-0-0-0-0-1-1-0-1",//5
        "0-1-0-1-0-0-1-0-1-0-1-1-0-1-1-1",//6
        "0-1-1-0-0-1-0-1-1-1-1-0-1-1-0-1",//7
        "0-0-0-0-0-0-1-1-0-1-1-1-0-1-1-1",//8
        "0-1-1-1-1-0-1-1-0-0-1-0-0-1-0-1",//9
        "0-0-1-1-0-1-0-1-1-0-0-0-0-1-0-0",//10
        "0-1-1-1-1-0-1-0-0-0-0-1-0-0-0-1",//11
        "1-1-0-0-1-0-1-0-0-1-0-0-0-0-1-0",//12
        "",//13
        "",//14
        "",//15
        "",//16
        "",//17
        "",//18
        "",//19
        "",//20
        "",//22
        "",//23
        "",//24
        "",//25
        "",//26
        "",//27
        "",//28
        "",//29
        "",//30
        "",//31
        "",//32
        "",//33
        "",//34
        "",//35
        "",//36
        "",//37
        "",//38
        "",//39
        "",//40
        "",//41
        "",//42
        "",//43
        "",//44
        "",//45
        "",//46
        "",//47
        "",//48
        "",//49
        "",//50
};

}
