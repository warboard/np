package np;

import data.Resource;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import static np.Constant.mapData;

/**
 *
 * @author Azhir-user
 */
public class NP extends Application {

    public static double size = 160;
    public static double gap = 10;

    Part p1 = new Part(Constant.FREE, Constant.BEAR, Constant.WHITE, Constant.FISH, Constant.TRUE_COLOR);
    Part p2 = new Part(Constant.FREE, Constant.FISH, Constant.BEAR, Constant.WHITE, Constant.TRUE_COLOR);
    Part p3 = new Part(Constant.FREE, Constant.BEAR, Constant.FISH, Constant.WHITE, Constant.TRUE_COLOR);
    Part p4 = new Part(Constant.BEAR, Constant.WHITE, Constant.WHITE, Constant.FREE, Constant.TRUE_COLOR);
    Part p5 = new Part(Constant.FISH, Constant.FISH, Constant.FREE, Constant.FREE, Constant.TRUE_COLOR);
    Part p6 = new Part(Constant.FISH, Constant.BEAR, Constant.FREE, Constant.FREE, Constant.TRUE_COLOR);

    static Map map;
    IntegerProperty mapNumber = new SimpleIntegerProperty(1);
    int maxNumber = 12;
    static double[] locX;
    static double[] locY;

    Group menuBox;
    Label number;
    static Button next;
    static Button back;
    static Button reSet;

    Rectangle dis;

    Timer timer;
    int startTime = 5;
    int timeStep = 60;

    static Label text;

    static AnchorPane root;

    static String[] win = {"ای ول دمت گرم", "کارت درسته", "دیگه وقتشه"};
    static String[] lose = {"یکم مانده بود", "حتما دانشجو هم هستی","دادش سیا ضایع شد"};

    
    ImageView  logo;
    
    
    
    @Override

    public void start(Stage primaryStage) {

        root = new AnchorPane();
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, 5 * gap + 4 * size, 3 * gap + 4 * size);

        System.out.println(5 * gap + 4 * size+"wwwwwwwwwwwwwwwwwwwwwwwwwww");
        System.out.println(3 * gap + 4 * size+"hhhhhhhhhhhhhhhhhhhhhhhhhhhh");
        
        
        dis = new Rectangle(5 * gap + 4 * size, 3 * gap + 4 * size, 5 * gap + 4 * size, 3 * gap + 4 * size);
        dis.setFill(Color.GREY);
        dis.setOpacity(0.5);

        map = new Map(3 * size, mapData[mapNumber.get()]);

        map.setLayoutX(gap);
        map.setLayoutY(size + 2 * gap);

        locX = new double[5];
        locY = new double[5];
        for (int i = 0; i < 5; i++) {
            locX[i] = size / 2 + gap + i * size / 2;
            locY[i] = size * 1.5 + 2 * gap + i * size / 2;
        }

        number = new Label();
        Font f = new Font(30);

        number.setFont(f);

        number.setTextFill(Color.DARKGREEN);
        number.textProperty().bind(mapNumber.asString());

        text = new Label("Lose");
        Font f2 = new Font(50);
        text.setFont(f2);
        text.setVisible(false);
        double k=3 * size;
        text.setPrefWidth(k);
        text.setMaxWidth(k);
        text.setMinWidth(k);
        text.setLayoutY(k);

        dis.visibleProperty().bind(text.visibleProperty());

        next = new Button("next");

        next.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        next();
                    }
                }
        );

        back = new Button("Back");

        back.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        back();
                    }
                }
        );

        reSet = new Button("ReSet");

        reSet.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {

                        reSet();

                    }

                }
        );

        back.setLayoutX(0);
        number.setLayoutX(50);
        next.setLayoutX(120);
        reSet.setLayoutX(45);

        reSet.setLayoutY(40);

        //next.prefHeightProperty().bind(number.prefHeightProperty());
        //rest.prefHeightProperty().bind(number.prefHeightProperty());
        next.setPrefHeight(
                70);
        back.setPrefHeight(
                70);
        reSet.setPrefHeight(
                30);

        reSet.setPrefWidth(
                70);

        menuBox = new Group();

        menuBox.setLayoutX(gap);

        menuBox.setLayoutY(gap);

        menuBox.setLayoutX(4 * gap + 3 * size);

        menuBox.getChildren().add(next);
        menuBox.getChildren().add(number);
        //menuBox.getChildren().add(back);
        menuBox.getChildren().add(reSet);

        p1.reSet();

        p2.reSet();

        p3.reSet();

        p4.reSet();

        p5.reSet();

        p6.reSet();

        root.getChildren().add(map.getPNode());

        root.getChildren().add(p1.getPNode());
        root.getChildren().add(p2.getPNode());
        root.getChildren().add(p3.getPNode());
        root.getChildren().add(p4.getPNode());
        root.getChildren().add(p5.getPNode());
        root.getChildren().add(p6.getPNode());
        root.getChildren().add(dis);
        root.getChildren().add(menuBox);
        root.getChildren().add(text);
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        timer = new Timer(startTime, 0, timeStep, -1);
        timer.getTimer().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                int t = newValue.intValue();
                if (t % 60 == 0) {
                    timer.getTimerView().shakeIt();
                }
                if (t < 60) {
                    timer.getTimerView().runHeart(true);
                }
                if (t == 0) {
                    endGame("endTime");
                }
            }
        });
        
        timer.getTimerView().setLayoutX(0);
        timer.getTimerView().setLayoutY(70 + gap);
        menuBox.getChildren().add(timer.getTimerView().getPNode());

        next.setDisable(true);
       // reSet.setDisable(true);
        
        logo=new ImageView(Resource.getImage("uni.png"));
        
        root.getChildren().add(logo);
        logo.fitWidthProperty().bind(scene.widthProperty());
        logo.fitHeightProperty().bind(scene.heightProperty() );
        
        
        
        FadeTransition fd=new FadeTransition(Duration.millis(1000));
        fd.setNode(logo);
        fd.setFromValue(1.0);
        fd.setToValue(0);
        
        Timeline tm= new Timeline(new KeyFrame(Duration.millis(1050), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                logo.setVisible(false);
                timer.play();
            }
        }));
        primaryStage.show();
        tm.play();
        fd.play();
        
        
        
    }

    public static void setLacation() {
        System.out.println("enterLoc");

        double lx = Part.BUFFER.getLayoutX() + size / 2;
        double ly = Part.BUFFER.getLayoutY() + size / 2;
        double r = size / 3;

        int ix = -1;
        int iy = -1;

        for (int i = 0; i < 5; i++) {
            if (locX[i] + r > lx && locX[i] - r < lx) {
                ix = i;
                break;
            }
        }
        for (int i = 0; i < 5; i++) {
            if (locY[i] + r > ly && locY[i] - r < ly) {
                iy = i;
                break;
            }
        }
        if (ix != -1 && iy != -1) {
            if (map.addPartToMap(ix, iy)) {
                Part.BUFFER.setIx(ix);
                Part.BUFFER.setIy(iy);
            } else {
                Part.BUFFER.getPNode().toFront();
            }
        } else {
            Part.BUFFER.reSet();
        }

    }

    public static void endGame(String s) {
        text.toFront();
        switch (s) {
            case "endTime": {
                text.setText(lose[((int) (Math.random() * 1000)) % lose.length]);
                text.setTextFill(Color.RED);
                text.setVisible(true);
                text.setAlignment(Pos.CENTER);
                reSet.setDisable(false);
                next.setDisable(true);
                break;
            }
            case "winGame": {
                text.setText(win[((int) (Math.random() * 1000)) % win.length]);
                text.setTextFill(Color.LIGHTGREEN);
                text.setVisible(true);
                text.setAlignment(Pos.CENTER);
                next.setDisable(false);
                break;
            }
        }
    }

    public void reSet() {
        p1.reSet();
        p2.reSet();
        p3.reSet();
        p4.reSet();
        p5.reSet();
        p6.reSet();
        mapNumber.set(1);
        map.setMapData(mapData[mapNumber.get()]);
        text.setVisible(false);
        timer.playFromStart();
    }

    public void next() {
        p1.reSet();
        p2.reSet();
        p3.reSet();
        p4.reSet();
        p5.reSet();
        p6.reSet();

        mapNumber.set(mapNumber.get() + 1 > maxNumber ? 1 : mapNumber.get() + 1);
        text.setVisible(false);
        map.setMapData(mapData[mapNumber.get()]);
        next.setDisable(true);
        map.drawInput();
    }

    public void back() {
        p1.reSet();
        p2.reSet();
        p3.reSet();
        p4.reSet();
        p5.reSet();
        p6.reSet();

        mapNumber.set(mapNumber.get() - 1 < 1 ? maxNumber : mapNumber.get() - 1);

        map.setMapData(mapData[mapNumber.get()]);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
