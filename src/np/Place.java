/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Arc;
import view.container.MyGroupCover;

/**
 *
 * @author Azhir-user
 */
public class Place extends MyGroupCover{
    int in0,in1,in2,in3;
    String out0,out1,out2,out3;

    Arc EH;
    
    double size;
    public Place(int []in, String []out ,double size) {
        this.in0 = in[0];
        this.in1 = in[1];
        this.in2 = in[2];
        this.in3 = in[3];
        this.out0 = out[0];
        this.out1 = out[1];
        this.out2 = out[2];
        this.out3 = out[3];
        
        this.size=size;
        EH=new Arc();
        EH.setStartAngle(0);
        EH.setLength(360);
        EH.setCenterX(size/2);
        EH.setCenterY(size/2);
        EH.setRadiusX(size/2);
        EH.setRadiusY(size/2);
        
        EH.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        getChildren().add(EH);
    }
    
    public void set(Part p){
        String s[][]=p.getP();
        if(in0!=-1){
            out0=s[0][0];
        }
        if(in1!=-1){
            out1=s[0][1];
        }
        if(in2!=-1){
            out2=s[1][0];
        }
        if(in3!=-1){
            out3=s[1][1];
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
