/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import com.sun.javafx.geom.Crossings;
import data.Resource;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import static np.NP.map;
import view.container.MyGroupCover;

/**
 *
 * @author Azhir-user
 */
public class Part extends MyGroupCover {

    String[][] p;
    ImageView[][] iv;
    Rectangle[][] b;
    ImageView EH;
    Line[][] border;

    public static Part BUFFER;
    public static double dx = 0;
    public static double dy = 0;
    public int ix = -1;
    public int iy = -1;
    private boolean draged = false;
    private Color color;

    private static int idCunter = 0;
    public final int id;

    public Part(String p00, String p01, String p10, String p11, Color c) {
        id = idCunter;
        idCunter++;
        p = new String[2][2];
        for (String[] i : p) {
            for (String j : i) {
                j = "";
            }
        }

        p[0][0] = p00;
        p[0][1] = p01;
        p[1][0] = p10;
        p[1][1] = p11;

        iv = new ImageView[2][];
        b = new Rectangle[2][];
        for (int i = 0; i < 2; i++) {
            iv[i] = new ImageView[2];
            b[i] = new Rectangle[2];
            for (int j = 0; j < 2; j++) {
                iv[i][j] = new ImageView();
                b[i][j] = new Rectangle();

                getChildren().add(b[i][j]);
                getChildren().add(iv[i][j]);
            }
        }
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                ImageView k = iv[i][j];
                k.setFitWidth(NP.size / 2);
                k.setFitHeight(NP.size / 2);
                k.setLayoutX(j * NP.size / 2);
                k.setLayoutY(i * NP.size / 2);

                Rectangle r = b[i][j];
                r.setWidth(NP.size / 2);
                r.setHeight(NP.size / 2);
                r.setLayoutX(j * NP.size / 2);
                r.setLayoutY(i * NP.size / 2);
                r.setFill(c);
                r.setOpacity(0.30);

            }
        }
        EH = new ImageView(Resource.getImage("Rotate.png"));
        EH.setFitWidth(NP.size / 4);
        EH.setFitHeight(NP.size / 4);

        EH.setLayoutX(NP.size / 2 - NP.size / 8);
        EH.setLayoutY(NP.size / 2 - NP.size / 8);
        EH.setOpacity(1.0);

        setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                BUFFER = This();
                dx = event.getSceneX() - getLayoutX();
                dy = event.getSceneY() - getLayoutY();
                NP.map.removePartFromMap(ix, iy);
                draged = false;
            }
        });

        setOnMouseDragged(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Part.BUFFER.setLayoutX(event.getSceneX() - Part.dx);
                Part.BUFFER.setLayoutY(event.getSceneY() - Part.dy);

                draged = true;
            }
        });
        setOnMouseReleased(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("111111111111111111111111111111");
                NP.setLacation();
            }
        });

        EH.setOnMouseReleased(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("222222222222222222222222222222222");

                if (!draged) {
                    if (event.getButton() == MouseButton.PRIMARY) {
                        NP.map.removePartFromMap(Part.BUFFER.ix, Part.BUFFER.iy);
                        Part.BUFFER.rotate();
                    } else if (event.getButton() == MouseButton.SECONDARY) {
                        NP.map.removePartFromMap(Part.BUFFER.ix, Part.BUFFER.iy);
                        Part.BUFFER.reSet();
                    }
                }
                NP.setLacation();
                draged = false;
                event.consume();
            }
        });

        Button b = new Button("111");
        b.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                map.drawInput();
            }
        });

        draw();
        drawBorder();
        checkBorder();
        getChildren().add(EH);
    }

    private void draw() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                iv[i][j].setImage(Resource.getImage(p[i][j] + ".png"));
                if (p[i][j].equals(Constant.FREE)) {
                    b[i][j].setVisible(false);
                } else {
                    b[i][j].setVisible(true);
                }
            }
        }
    }

    private void drawBorder() {
        border = new Line[4][];
        for (int i = 0; i < 4; i++) {
            border[i] = new Line[4];
        }
        double lx = iv[0][0].getLayoutX();
        double ly = iv[0][0].getLayoutY();
        border[0][0] = new Line(lx, ly, lx + NP.size / 2, ly);
        border[0][1] = new Line(lx + NP.size / 2, ly, lx + NP.size / 2, ly + NP.size / 2);
        border[0][2] = new Line(lx + NP.size / 2, ly + NP.size / 2, lx, ly + NP.size / 2);
        border[0][3] = new Line(lx, ly + NP.size / 2, lx, ly);

        lx = iv[0][1].getLayoutX();
        ly = iv[0][1].getLayoutY();
        border[1][0] = new Line(lx, ly, lx + NP.size / 2, ly);
        border[1][1] = new Line(lx + NP.size / 2, ly, lx + NP.size / 2, ly + NP.size / 2);
        border[1][2] = new Line(lx + NP.size / 2, ly + NP.size / 2, lx, ly + NP.size / 2);
        border[1][3] = new Line(lx, ly + NP.size / 2, lx, ly);

        lx = iv[1][0].getLayoutX();
        ly = iv[1][0].getLayoutY();
        border[2][0] = new Line(lx, ly, lx + NP.size / 2, ly);
        border[2][1] = new Line(lx + NP.size / 2, ly, lx + NP.size / 2, ly + NP.size / 2);
        border[2][2] = new Line(lx + NP.size / 2, ly + NP.size / 2, lx, ly + NP.size / 2);
        border[2][3] = new Line(lx, ly + NP.size / 2, lx, ly);

        lx = iv[1][1].getLayoutX();
        ly = iv[1][1].getLayoutY();
        border[3][0] = new Line(lx, ly, lx + NP.size / 2, ly);
        border[3][1] = new Line(lx + NP.size / 2, ly, lx + NP.size / 2, ly + NP.size / 2);
        border[3][2] = new Line(lx + NP.size / 2, ly + NP.size / 2, lx, ly + NP.size / 2);
        border[3][3] = new Line(lx, ly + NP.size / 2, lx, ly);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                border[i][j].setStroke(Color.BLACK);
                border[i][j].setStrokeWidth(5);
                
                getChildren().add(border[i][j]);
            }
        }

    }

    private Part This() {
        return this;
    }

    private void rotate() {
        String x1, x2, x3, x4;
        x1 = p[0][0];
        x2 = p[0][1];
        x3 = p[1][0];
        x4 = p[1][1];

        p[0][0] = x3;
        p[0][1] = x1;
        p[1][0] = x4;
        p[1][1] = x2;

        draw();
        checkBorder();
    }

    private void checkBorder() {

        for (int m = 0; m < 2; m++) {
            for (int n = 0; n < 2; n++) {
                if (!p[m][n].equals(Constant.FREE)) {
                    for (int i = 0; i < 4; i++) {
                        border[2 * m + n][i].setVisible(true);
                    }
                } else {
                    for (int i = 0; i < 4; i++) {
                        border[2 * m + n][i].setVisible(false);
                    }
                }
            }
        }
        
        if(!p[0][0].equals(Constant.FREE) && !p[0][1].equals(Constant.FREE)){
            border[0][1].setVisible(false);
            border[1][3].setVisible(false);
        }
        if(!p[0][0].equals(Constant.FREE) && !p[1][0].equals(Constant.FREE)){
            border[0][2].setVisible(false);
            border[2][0].setVisible(false);
        }
        if(!p[0][1].equals(Constant.FREE) && !p[1][1].equals(Constant.FREE)){
            border[1][2].setVisible(false);
            border[3][0].setVisible(false);
        }
        if(!p[1][0].equals(Constant.FREE) && !p[1][1].equals(Constant.FREE)){
            border[2][1].setVisible(false);
            border[3][3].setVisible(false);
        }
        
        
        
        
    }

    public void reSet() {
        ix = -1;
        iy = -1;
        if (id < 3) {
            setLayoutX(NP.gap + (NP.gap + NP.size) * id);
            setLayoutY(NP.gap);
        } else {
            setLayoutX(NP.gap + (NP.gap + NP.size) * 3);
            setLayoutY(NP.gap + (NP.gap + NP.size) * (id - 2));
        }
    }

    public void setCenterLoc(double cx, double cy) {
        setLayoutX(cx - NP.size / 2);
        setLayoutY(cy - NP.size / 2);
    }

    public String[][] getP() {
        return p;
    }

    public int getIx() {
        return ix;
    }

    public void setIx(int ix) {
        setLayoutX(NP.size / 2 + NP.gap + ix * NP.size / 2 - NP.size / 2);
        this.ix = ix;
    }

    public int getIy() {
        return iy;
    }

    public void setIy(int iy) {
        setLayoutY(NP.size * 1.5 + 2 * NP.gap + iy * NP.size / 2 - NP.size / 2);
        this.iy = iy;
    }

    void setColor(Color c) {
        color = c;
        for (Rectangle[] ri : b) {
            for (Rectangle rii : ri) {
                rii.setFill(c);
            }
        }
    }

    public Color getColor() {
        return color;
    }

}
