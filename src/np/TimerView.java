/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.effect.Glow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import view.container.MyGroup;
import view.container.MyGroupCover;

/**
 *
 * @author Azhir-user
 */
public class TimerView extends MyGroupCover {

    SevenSegment ss1;
    SevenSegment ss2;
    SevenSegment ss3;
    SevenSegment ss4;
    Group dots;

    Rectangle backGround;

    ParallelTransition heart;
    double scale1 = 1;
    double scale2 = 1.1;
    double scale3 = 1.15;
    boolean hearting = false;

    ParallelTransition shake;
    double angle1 = -10;
    double angle2 = 0;
    double angle3 = 10;
    boolean shaking = false;

    double size = 25;
    double gap = size / 6;
    double dotGap = 7;
    double dotSize = 7;

    public TimerView(int min, int sec, Color onColor, Color offColor) {
        group = new Group();

        heart = new ParallelTransition();
        heart.setCycleCount(Timeline.INDEFINITE);

        shake = new ParallelTransition();
        shake.setCycleCount(Timeline.INDEFINITE);

        Glow onEffect = new Glow(1.7f);
        onEffect.setInput(new InnerShadow());
        // create effect for on dot LEDs
        Glow onDotEffect = new Glow(1.7f);
        onDotEffect.setInput(new InnerShadow(5, Color.BLACK));
        // create effect for off LEDs
        InnerShadow offEffect = new InnerShadow();

        ss1 = new SevenSegment(size, onColor, offColor, onEffect, offEffect);
        ss2 = new SevenSegment(size, onColor, offColor, onEffect, offEffect);
        ss3 = new SevenSegment(size, onColor, offColor, onEffect, offEffect);
        ss4 = new SevenSegment(size, onColor, offColor, onEffect, offEffect);

        dots = new Group(
                new Circle(dotSize / 2, dotSize / 2, dotSize, onColor),
                new Circle(dotSize / 2, 3.33 * dotSize + dotSize / 2, dotSize, onColor));
        dots.setEffect(onDotEffect);

        backGround = new Rectangle(5 * gap + 1.5 * dotGap + 4 * size * scale3 + dotSize, size * 2 * scale3, Color.TRANSPARENT);

        setParam(ss1);
        setParam(ss2);
        setParam(ss3);
        setParam(ss4);

        ss1.setLayoutX(gap);
        ss2.setLayoutX(3 * gap + size);
        ss3.setLayoutX(5 * gap + 2 * dotGap + 2 * size + dotSize);
        ss4.setLayoutX(7 * gap + 2 * dotGap + 3 * size + dotSize);
        dots.setLayoutX(4 * gap + dotGap + 2 * size);

        ss1.setLayoutY(size * (scale3 - 1));
        ss2.setLayoutY(size * (scale3 - 1));
        ss3.setLayoutY(size * (scale3 - 1));
        ss4.setLayoutY(size * (scale3 - 1));

        dots.setLayoutY(((size * 2 - 4.33 * dotSize) / 2) + (size * (scale3 - 1)));

        getChildren().add(backGround);
        getChildren().add(ss1);
        getChildren().add(ss2);
        getChildren().add(ss3);
        getChildren().add(ss4);
        getChildren().add(dots);

        setTime(min, sec);

    }

    public final void setTime(int min, int sec) {
        ss4.setNumber(sec % 10);
        ss3.setNumber(sec / 10);
        ss2.setNumber(min % 10);
        ss1.setNumber(min / 10);
    }

    private void setParam(SevenSegment s) {

        ScaleTransition st1 = new ScaleTransition(Duration.millis(230), s);
        st1.setFromX(scale1);
        st1.setFromY(scale1);
        st1.setToX(scale3);
        st1.setToY(scale3);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(135), s);
        st2.setFromX(scale3);
        st2.setFromY(scale3);
        st2.setToX(scale2);
        st2.setToY(scale2);

        ScaleTransition st3 = new ScaleTransition(Duration.millis(135), s);
        st3.setFromX(scale2);
        st3.setFromY(scale2);
        st3.setToX(scale3);
        st3.setToY(scale3);

        ScaleTransition st4 = new ScaleTransition(Duration.millis(500), s);
        st4.setFromX(scale3);
        st4.setFromY(scale3);
        st4.setToX(scale1);
        st4.setToY(scale1);

        SequentialTransition heartSepuTrans = new SequentialTransition(st1, st2, st3, st4);

        heart.getChildren().add(heartSepuTrans);

        RotateTransition rt1 = new RotateTransition(Duration.millis(200), s);
        rt1.setByAngle(angle2);
        rt1.setToAngle(angle1);

        RotateTransition rt2 = new RotateTransition(Duration.millis(200), s);
        rt2.setByAngle(angle1);
        rt2.setToAngle(angle3);

        RotateTransition rt3 = new RotateTransition(Duration.millis(200), s);
        rt3.setByAngle(angle3);
        rt3.setToAngle(angle1);

        RotateTransition rt4 = new RotateTransition(Duration.millis(200), s);
        rt4.setByAngle(angle1);
        rt4.setToAngle(angle3);

        RotateTransition rt5 = new RotateTransition(Duration.millis(200), s);
        rt5.setByAngle(angle3);
        rt5.setToAngle(angle2);

        SequentialTransition shakeSepuTrans = new SequentialTransition(rt1, rt2, rt3, rt4, rt5);
        shake.getChildren().add(shakeSepuTrans);
    }

    public void runHeart(boolean b) {
        hearting = b;
        if (b) {
            heart.play();
        } else {
            heart.stop();
        }
    }

    public boolean isHearting() {
        return hearting;
    }

    public void runShake(boolean b) {
        shaking = b;
        if (b) {
            shake.setCycleCount(Timeline.INDEFINITE);
            shake.play();

        } else {
            shake.stop();
        }
    }

    public boolean isShaking() {
        return shaking;
    }

    public void shakeIt() {
        shake.setCycleCount(1);
        shake.play();
    }
}
