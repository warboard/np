/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import javafx.scene.Parent;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Shear;

/**
 *
 * @author Azhir-user
 */
public class SevenSegment extends Parent {

    private static final boolean[][] DIGIT_COMBINATIONS = new boolean[][]{
        new boolean[]{true, false, true, true, true, true, true},
        new boolean[]{false, false, false, false, true, false, true},
        new boolean[]{true, true, true, false, true, true, false},
        new boolean[]{true, true, true, false, true, false, true},
        new boolean[]{false, true, false, true, true, false, true},
        new boolean[]{true, true, true, true, false, false, true},
        new boolean[]{true, true, true, true, false, true, true},
        new boolean[]{true, false, false, false, true, false, true},
        new boolean[]{true, true, true, true, true, true, true},
        new boolean[]{true, true, true, true, true, false, true}};
    private Polygon[] polygons;

    private final Color onColor;
    private final Color offColor;
    private final Effect onEffect;
    private final Effect offEffect;

    int number = 0;

    public SevenSegment(double size, Color onColor, Color offColor, Effect onEffect, Effect offEffect) {
        this.onColor = onColor;
        this.offColor = offColor;
        this.onEffect = onEffect;
        this.offEffect = offEffect;

        double w = size;
        double h = 2 * w;

        polygons = new Polygon[]{
            new Polygon(0.037 * w, 0.000 * h,
            0.962 * w, 0.000 * h,
            0.777 * w, 0.092 * h,
            0.222 * w, 0.092 * h),
            new Polygon(0.222 * w, 0.453 * h,
            0.777 * w, 0.453 * h,
            0.962 * w, 0.500 * h,
            0.777 * w, 0.546 * h,
            0.222 * w, 0.546 * h,
            0.037 * w, 0.500 * h),
            new Polygon(0.222 * w, 0.907 * h,
            0.777 * w, 0.907 * h,
            0.962 * w, 1.000 * h,
            0.037 * w, 1.000 * h),
            new Polygon(0.000 * w, 0.018 * h,
            0.185 * w, 0.111 * h,
            0.185 * w, 0.435 * h,
            0.000 * w, 0.481 * h),
            new Polygon(0.814 * w, 0.111 * h,
            1.000 * w, 0.018 * h,
            1.000 * w, 0.481 * h,
            0.814 * w, 0.435 * h),
            new Polygon(0.000 * w, 0.518 * h,
            0.185 * w, 0.564 * h,
            0.185 * w, 0.888 * h,
            0.000 * w, 0.981 * h),
            new Polygon(0.814 * w, 0.564 * h,
            1.000 * w, 0.518 * h,
            1.000 * w, 0.981 * h,
            0.814 * w, 0.888 * h)};

        getChildren().addAll(polygons);
        showNumber(1);
    }

    public void setSize(double size) {
        double w = Math.sqrt(size * size / 5);
        double h = 2 * w;
        getChildren().clear();
        polygons = new Polygon[]{
            new Polygon(0.037 * w, 0.000 * h,
            0.962 * w, 0.000 * h,
            0.777 * w, 0.092 * h,
            0.222 * w, 0.092 * h),
            new Polygon(0.222 * w, 0.453 * h,
            0.777 * w, 0.453 * h,
            0.962 * w, 0.500 * h,
            0.777 * w, 0.546 * h,
            0.222 * w, 0.546 * h,
            0.037 * w, 0.500 * h),
            new Polygon(0.222 * w, 0.907 * h,
            0.777 * w, 0.907 * h,
            0.962 * w, 1.000 * h,
            0.037 * w, 1.000 * h),
            new Polygon(0.000 * w, 0.018 * h,
            0.185 * w, 0.111 * h,
            0.185 * w, 0.435 * h,
            0.000 * w, 0.481 * h),
            new Polygon(0.814 * w, 0.111 * h,
            1.000 * w, 0.018 * h,
            1.000 * w, 0.481 * h,
            0.814 * w, 0.435 * h),
            new Polygon(0.000 * w, 0.518 * h,
            0.185 * w, 0.564 * h,
            0.185 * w, 0.888 * h,
            0.000 * w, 0.981 * h),
            new Polygon(0.814 * w, 0.564 * h,
            1.000 * w, 0.518 * h,
            1.000 * w, 0.981 * h,
            0.814 * w, 0.888 * h)};

        getChildren().addAll(polygons);
        showNumber(number);
    }
    
    public void showNumber(Integer num) { 
        if (num < 0 || num > 9) {
            num = 0; // default to 0 for non-valid numbers
        }
        for (int i = 0; i < 7; i++) {
            polygons[i].setFill(DIGIT_COMBINATIONS[num][i] ? onColor : offColor);
            polygons[i].setEffect(DIGIT_COMBINATIONS[num][i] ? onEffect : offEffect);
            polygons[i].setOpacity(DIGIT_COMBINATIONS[num][i] ? 1 : 0.2);
        }
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {

        this.number = number;
        showNumber(number);
    }
    
    
    
}
