/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 *
 * @author Azhir-user
 */
public class Timer {

    public TimerView timerView;

    public int startTime;
    public int endTime;
    public int timeSingle;
    public int timerStep;
    public IntegerProperty timer;
    public Timeline timerCounter;

    public Timer(int startTime, int endTime, int timeSingle, int timerStep) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.timeSingle = timeSingle;
        this.timerStep = timerStep;

        timer = new SimpleIntegerProperty(startTime * timeSingle);
        timerCounter = new Timeline(new KeyFrame(Duration.millis(1000), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (timer.getValue() == endTime * timeSingle) {
                    timerCounter.stop();
                }
                timer.set(timer.getValue() + timerStep);
                timerView.setTime(timer.get() / 60, timer.get() % 60);
            }
        }));
        timerCounter.setCycleCount(Timeline.INDEFINITE);
        timerView = new TimerView((startTime*timeSingle)/60, (startTime*timeSingle)%60, Color.RED, Color.GREY);
    }

    public TimerView getTimerView() {
        return timerView;
    }

    public void setTimerView(TimerView timerView) {
        this.timerView = timerView;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public int getTimeSingle() {
        return timeSingle;
    }

    public void setTimeSingle(int timeSingle) {
        this.timeSingle = timeSingle;
    }

    public int getTimerStep() {
        return timerStep;
    }

    public void setTimerStep(int timerStep) {
        this.timerStep = timerStep;
    }

    public IntegerProperty getTimer() {
        return timer;
    }

    public void setTimer(IntegerProperty timer) {
        this.timer = timer;
    }

    public void playFromStart() {
        timer.set(startTime*timeSingle);
        timerCounter.playFromStart();
    }

    public void pause() {
        timerCounter.pause();
    }
    
    public void play(){
        timerCounter.play();
    }

    public void stop() {
        timerCounter.stop();
        timer.set(startTime*timeSingle);
    }

}
