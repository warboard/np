/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np;

import data.Resource;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import view.container.MyGroupCover;

/**
 *
 * @author Azhir-user
 */
public class Map extends MyGroupCover {

    double size;
    String map[][];
    String[][] input;
    ImageView iv[][];
    Rectangle bord;

    //Place [][]p;
    String part[][];

    public Map(double size, String mapData) {
        this.size = size;
        map = new String[4][];
        input = new String[4][];
        iv = new ImageView[4][];

        String[] sVector = mapData.split("-");

        bord = new Rectangle(size, size, Color.TRANSPARENT);
        bord.setStroke(Color.BLACK);
        getChildren().add(bord);

        for (int i = 0; i < 4; i++) {
            map[i] = new String[4];
            input[i] = new String[4];
            iv[i] = new ImageView[4];
            for (int j = 0; j < 4; j++) {
                map[i][j] = sVector[i * 4 + j].equals("0") ? Constant.WATER : Constant.ICE;
                input[i][j] = Constant.FREE;
                iv[i][j] = new ImageView();
                ImageView ivi = iv[i][j];

                ivi.setImage(Resource.getImage(map[i][j] + ".png"));
                ivi.setFitWidth(size / 6);
                ivi.setFitHeight(size / 6);

                ivi.setLayoutX(j * size / 6 + size / 6);
                ivi.setLayoutY(i * size / 6 + size / 6);

                getChildren().add(ivi);
            }
        }

    }

    public void setMapData(String mapData) {
        String[] sVector = mapData.split("-");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                map[i][j] = sVector[i * 4 + j].equals("0") ? Constant.WATER : Constant.ICE;
                input[i][j] = Constant.FREE;
                ImageView ivi = iv[i][j];
                ivi.setImage(Resource.getImage(map[i][j] + ".png"));
            }
        }
    }

    public boolean addPartToMap(int i, int j) {
        boolean flag = true;
        System.out.println(Part.BUFFER.p[0][0] + "      " + Part.BUFFER.p[0][1]);
        System.out.println(Part.BUFFER.p[1][0] + "      " + Part.BUFFER.p[1][1]);

        System.out.println(i + "      " + j);

        if (j - 1 > -1 && j < 4) {
            if (i - 1 > -1 && i < 4) {
                System.out.println(input[j - 1][i - 1] + "     " + input[j - 1][i]);

                System.out.println(input[j][i - 1] + "    " + input[j][i]);
            }
        }

        if (j != -1 && i != -1) {
            if (!Part.BUFFER.p[0][0].equals(Constant.FREE) && j - 1 > -1 && i - 1 > -1 && !input[j - 1][i - 1].equals(Constant.FREE)) {
                flag = false;
            }
            if (!Part.BUFFER.p[0][1].equals(Constant.FREE) && j - 1 > -1 && i < 4 && !input[j - 1][i].equals(Constant.FREE)) {
                flag = false;
            }
            if (!Part.BUFFER.p[1][0].equals(Constant.FREE) && j < 4 && i - 1 > -1 && !input[j][i - 1].equals(Constant.FREE)) {
                flag = false;
            }
            if (!Part.BUFFER.p[1][1].equals(Constant.FREE) && j < 4 && i < 4 && !input[j][i].equals(Constant.FREE)) {
                flag = false;
            }
        }
        if (!flag) {
            Part.BUFFER.setColor(Constant.FALSE_COLOR);
            return false;
        }
        Part.BUFFER.setColor(Constant.TRUE_COLOR);
        if (j != -1 && i != -1) {
            if (!Part.BUFFER.p[0][0].equals(Constant.FREE) && j - 1 > -1 && i - 1 > -1) {
                input[j - 1][i - 1] = Part.BUFFER.p[0][0];
            }
            if (!Part.BUFFER.p[0][1].equals(Constant.FREE) && j - 1 > -1 && i < 4) {
                input[j - 1][i] = Part.BUFFER.p[0][1];
            }
            if (!Part.BUFFER.p[1][0].equals(Constant.FREE) && j < 4 && i - 1 > -1) {
                input[j][i - 1] = Part.BUFFER.p[1][0];
            }
            if (!Part.BUFFER.p[1][1].equals(Constant.FREE) && j < 4 && i < 4) {
                input[j][i] = Part.BUFFER.p[1][1];
            }
        }
        System.out.println("->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->");
        for (String[] si : input) {
            System.out.println(si[0] + "  " + si[1] + "  " + si[2] + "  " + si[3]);
        }
        System.out.println("->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->->");
        if (check()) {
            NP.endGame("winGame");
        } else {
            System.out.println("lose");
        }
        return true;

    }

    public void removePartFromMap(int i, int j) {
        System.out.println("clear" + i + "    " + j);
        System.out.println("clear" + Part.BUFFER.ix + "    " + Part.BUFFER.iy);

        System.out.println(!Part.BUFFER.p[0][0].equals(Constant.FREE) + "      " + !Part.BUFFER.p[0][1].equals(Constant.FREE));
        System.out.println(!Part.BUFFER.p[1][0].equals(Constant.FREE) + "      " + !Part.BUFFER.p[1][1].equals(Constant.FREE));

        System.out.println("##################################");
        for (String[] si : input) {
            System.out.println(si[0] + "  " + si[1] + "  " + si[2] + "  " + si[3]);
        }
        System.out.println("#######################################");
        if (Part.BUFFER.getColor() == Constant.TRUE_COLOR) {
            if (j != -1 && i != -1) {
                if (!Part.BUFFER.p[0][0].equals(Constant.FREE) && j - 1 > -1 && i - 1 > -1) {
                    input[j - 1][i - 1] = Constant.FREE;
                }
                if (!Part.BUFFER.p[0][1].equals(Constant.FREE) && j - 1 > -1 && i < 4) {
                    input[j - 1][i] = Constant.FREE;
                }
                if (!Part.BUFFER.p[1][0].equals(Constant.FREE) && j < 4 && i - 1 > -1) {
                    input[j][i - 1] = Constant.FREE;
                }
                if (!Part.BUFFER.p[1][1].equals(Constant.FREE) && j < 4 && i < 4) {
                    input[j][i] = Constant.FREE;
                }
            }
        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for (String[] si : input) {
            System.out.println(si[0] + "  " + si[1] + "  " + si[2] + "  " + si[3]);
        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    public boolean check() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (input[i][j].equals(Constant.FREE)) {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!"+false);
                    return false;
                }
                if (input[i][j].equals(Constant.BEAR) && !map[i][j].equals(Constant.ICE)) {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!"+false);
                    return false;
                }
                if (input[i][j].equals(Constant.FISH) && !map[i][j].equals(Constant.WATER)) {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!"+false);
                    return false;
                }
            }
        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!"+true);
        return true;
    }

    public void drawInput() {
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        for (String[] si : input) {
            System.out.println(si[0] + "  " + si[1] + "  " + si[2] + "  " + si[3]);
        }
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    }
}
